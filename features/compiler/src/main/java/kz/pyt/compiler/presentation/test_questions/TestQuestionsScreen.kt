package kz.pyt.compiler.presentation.test_questions

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlinx.parcelize.Parcelize
import kz.pyt.ui.DestinationController
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.base.Destination
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.ext.scaledClickable
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CommonButton
import org.koin.androidx.compose.getViewModel

@Parcelize
class TestQuestionsDestination : Destination {
    @Composable
    override fun Content(controller: DestinationController) {
        TestQuestionsScreen()
    }
}

@Composable
fun TestQuestionsScreen() {
    val viewModel = getViewModel<TestQuestionsViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF0A4F4F))
            .safeStatusBarPadding(),
    ) {

        Spacer(modifier = Modifier.height(16.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(1f)
                .background(
                    LocalAppTheme.colors.white,
                    RoundedCornerShape(topStart = 36.dp, topEnd = 36.dp)
                )
                .padding(horizontal = 16.dp)
        ) {

            Spacer(modifier = Modifier.padding(10.dp))

            Text(
                text = state.currentQuestion,
                style = LocalAppTheme.typography.l18B,
                color = LocalAppTheme.colors.primaryText
            )

            Spacer(modifier = Modifier.padding(20.dp))

            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                AnswerComponent(
                    answerChar = "A",
                    answer = state.answer[0],
                    selected = state.selectedAnswer.equals(state.answer[0]),
                    onClick = viewModel::onUpdateSelected
                )
                Spacer(modifier = Modifier.padding(8.dp))
                AnswerComponent(
                    answerChar = "B",
                    answer = state.answer[1],
                    selected = state.selectedAnswer.equals(state.answer[1]),
                    onClick = viewModel::onUpdateSelected
                )
                Spacer(modifier = Modifier.padding(8.dp))
                AnswerComponent(
                    answerChar = "C",
                    answer = state.answer[2],
                    selected = state.selectedAnswer.equals(state.answer[2]),
                    onClick = viewModel::onUpdateSelected
                )
                Spacer(modifier = Modifier.padding(8.dp))
                AnswerComponent(
                    answerChar = "D",
                    answer = state.answer[3],
                    selected = state.selectedAnswer.equals(state.answer[3]),
                    onClick = viewModel::onUpdateSelected
                )
            }

            Spacer(modifier = Modifier.padding(20.dp))

            CommonButton(
                text = "Next",
                modifier = Modifier.fillMaxWidth(),
                buttonColors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF0A4F4F),
                    disabledBackgroundColor = LocalAppTheme.colors.bgButtonSecondary,
                ),
                isEnabled = !state.selectedAnswer.equals(""),
                onClick = viewModel::onClickNext,
            )
        }
    }
}

@Composable
fun AnswerComponent(
    answerChar: String,
    answer: String,
    selected: Boolean,
    onClick: (String) -> Unit
) {
    val color = if (selected) Color(0xFF0A4F4F) else LocalAppTheme.colors.accentText
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .scaledClickable {
                onClick(answer)
            },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Column(
            modifier = Modifier
                .background(color, RoundedCornerShape(14.dp))
        ) {
            Text(
                text = answerChar,
                style = LocalAppTheme.typography.l14B,
                color = LocalAppTheme.colors.white,
                modifier = Modifier
                    .padding(horizontal = 18.dp, vertical = 14.dp)

            )
        }

        Column(
            modifier = Modifier,
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = answer,
                style = LocalAppTheme.typography.l16B,
                color = Color(0xFF0A4F4F)
            )
        }
    }
}