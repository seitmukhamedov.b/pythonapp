package kz.pyt.compiler.presentation.test_questions

import kz.mmq.test.presentation.test_answer.TestAnswerDestination
import kz.pyt.ui.base.navigation.NavigateTo
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState
import java.time.LocalTime

data class TestQuestionsState(
    val counter: LocalTime,
    val questions: List<String> = emptyList(),
    val currentQuestionIndex: Int = 1,
    val currentQuestion: String = "",
    val answer: List<String> = emptyList(),
    val correctAnswer: List<String> = emptyList(),
    val answers: List<List<String>> = emptyList(),
    val selectedAnswer: String = "",
    val totalPoints: Int = 0,
) : UiState

class TestQuestionsViewModel : BaseViewModel<TestQuestionsState>() {
    override fun createInitialState() = TestQuestionsState(
        counter = LocalTime.now(),
        questions = listOf(
            "1)What is a good practice to avoid falling into debt?",
            "2)Which of the following can help improve your credit score?",
            "3)What should you avoid if you want to maintain a good credit score?"
        ),
        answers = listOf(
            listOf("Only pay the minimum balance on credit cards", "Borrow money for every large purchase", "Spend less than you earn", "Ignore your bills and hope they go away."),
            listOf("Missing monthly payments", "Using up your full credit limit every month", "Paying off your credit card balance in full and on time", "Opening multiple credit cards at once and not using them."),
            listOf("Paying bills on time", "Maximizing your credit card limit every month.", "Ignoring your credit report for errors.", "Applying for multiple loans in a short time.")
        ),
        correctAnswer = listOf(
            "Only pay the minimum balance on credit cards",
            "Paying off your credit card balance in full and on time",
            "Paying bills on time"
        ),
        currentQuestion = "1)What is a good practice to avoid falling into debt?",
        answer = listOf("Only pay the minimum balance on credit cards", "Borrow money for every large purchase", "Spend less than you earn", "Ignore your bills and hope they go away."),
        currentQuestionIndex = 1
    )

    fun onClickNext() {
        val points = if(currentState.selectedAnswer == currentState.correctAnswer[currentState.currentQuestionIndex-1]) 1 else 0
        setState {
            copy(
                currentQuestionIndex = currentQuestionIndex + 1,
                totalPoints = currentState.totalPoints + points,
            )
        }
        if(currentState.currentQuestionIndex > 3){
            navigate(NavigateTo(TestAnswerDestination(currentState.totalPoints)))
            return
        }
        setState {
            copy(
                currentQuestion = questions[currentQuestionIndex - 1],
                answer = answers[currentQuestionIndex - 1],
                selectedAnswer = ""
            )
        }
    }


    fun onUpdateSelected(answer: String) {
        setState { copy(selectedAnswer = answer) }
    }

}