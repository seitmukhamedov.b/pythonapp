package kz.pyt.compiler.presentation.lesson

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.parcelize.Parcelize
import kz.pyt.compiler.presentation.TestViewModel
import kz.pyt.ui.DestinationController
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.base.Destination
import kz.pyt.ui.ext.safeNavigationPadding
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CommonButton
import org.koin.androidx.compose.getViewModel

@Parcelize
class LessonDestination : Destination {
    @Composable
    override fun Content(controller: DestinationController) {
        LessonScreen()
    }
}

@Composable
private fun LessonScreen() {
    val viewModel = getViewModel<TestViewModel>()
    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF0A4F4F))
            .safeStatusBarPadding(),
    ) {

        Spacer(modifier = Modifier.padding(10.dp))

        Text(
            text = "Budgeting Basics",
            style = LocalAppTheme.typography.l24,
            color = LocalAppTheme.colors.primaryText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 10.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .background(Color(0xFFF1FFF3), RoundedCornerShape(20.dp))
                .padding(10.dp)
        )

        Spacer(modifier = Modifier.height(30.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(1f)
                .background(
                    Color(0xFFF1FFF3),
                    RoundedCornerShape(topStart = 36.dp, topEnd = 36.dp)
                )
                .padding(horizontal = 16.dp)
                .verticalScroll(rememberScrollState())
                .safeNavigationPadding()
        ) {
            Spacer(modifier = Modifier.padding(15.dp))

            Text(
                text = "Building an effective budget often starts by assessing your net income or take-home pay. That’s your total wages or salary after taking out taxes and employee benefits, such as 401(k) contributions and health insurance premiums. It’s the amount that is deposited in your bank account every pay period.\n" +
                        "\n" +
                        "Be careful not to focus on your total pay. You may end up overspending because you think you have more money available than you do. If you’re a freelancer, gig worker, contractor or are self-employed and your income is irregular, make sure to keep detailed notes of your contracts and pay.",
                style = LocalAppTheme.typography.l18,
                color = LocalAppTheme.colors.primaryText
            )

            Text(
                text = "Once you know how much money you have coming in, the next step is to figure out where it’s going. Tracking and categorizing your expenses can help you determine what you are spending the most money on and where it might be easiest to save. The more detail you gather, the better. For several weeks, record all your daily spending. Use whatever’s handy—an app on your smartphone, budgeting spreadsheet, online template, even pen and paper. Credit card and bank statements are a good place to start because they itemize your spending and often group transactions into broad categories, such as utilities and entertainment.\n" +
                        "\n" +
                        "Then group your fixed expenses together. These are regular monthly bills such as rent or mortgage, utilities and car payments. Next list your variable expenses—those that may change from month to month, such as groceries, gas and entertainment. This is an area where you might find opportunities to cut back.",
                style = LocalAppTheme.typography.l18,
                color = LocalAppTheme.colors.primaryText
            )

            Spacer(modifier = Modifier.padding(15.dp))

            CommonButton(
                text = "QUIZ",
                textColor = Color(0xFFDFF7E2),
                modifier = Modifier.fillMaxWidth(),
                buttonColors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF0A4F4F),
                    disabledBackgroundColor = LocalAppTheme.colors.bgButtonSecondary,
                ),
                onClick = viewModel::onClickStart,
            )
        }

    }
}