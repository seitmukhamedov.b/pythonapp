package kz.pyt.compiler.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kz.pyt.employee.checklist.CompilerContentDelegate
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.ext.scaledClickable
import kz.pyt.ui.theme.LocalAppTheme
import org.koin.androidx.compose.getViewModel

object TestDelegateImpl : CompilerContentDelegate {
    @Composable
    override fun Content() {
        TestScreen()
    }
}

@Composable
fun TestScreen() {
    val viewModel = getViewModel<TestViewModel>()
    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF0A4F4F))
            .safeStatusBarPadding(),
    ) {

        Spacer(modifier = Modifier.padding(10.dp))

        Text(
            text = "Our app has special lessons for you to improve your financial literacy. You can study the lessons and then take a test.",
            style = LocalAppTheme.typography.l15B,
            color = LocalAppTheme.colors.white,
            modifier = Modifier
                .padding(top = 10.dp)
                .padding(horizontal = 16.dp)
        )

        Spacer(modifier = Modifier.height(30.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(1f)
                .background(
                    Color(0xFFF1FFF3),
                    RoundedCornerShape(topStart = 36.dp, topEnd = 36.dp)
                )
                .padding(horizontal = 16.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Spacer(modifier = Modifier.padding(15.dp))

            TestComponent(
                modifier = Modifier
                    .scaledClickable { viewModel.onClickLesson() },
                number = 1,
                title = "Budgeting Basics",
                description = "Learn how to create a budget, track income and expenses, and take control of your finances.",
            )

            Spacer(Modifier.height(30.dp))

            TestComponent(
                number = 2,
                title = "Effective Saving Strategies",
                description = "Discover savings strategies that can help you achieve your financial goals.",
            )

            Spacer(Modifier.height(30.dp))

            TestComponent(
                number = 3,
                title = "Investing for Beginners",
                description = "Understand the basics of investing and how to\n" +
                        " make your money work for you.",
            )

            Spacer(Modifier.height(30.dp))

            TestComponent(
                number = 4,
                title = "Financial Planning for the Future",
                description = "Learn how to create long-term plans and set financial goals.",
            )

            Spacer(Modifier.height(30.dp))

            TestComponent(
                number = 5,
                title = "Avoiding Debt and Managing Credit",
                description = "Get familiar with practices that help you avoid debt a\n" +
                        "nd manage credit responsibly.",
            )

            Spacer(Modifier.height(30.dp))

        }

    }
}

@Composable
private fun TestComponent(
    modifier: Modifier = Modifier,
    number: Int,
    title: String,
    description: String,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xFF0A4F4F), RoundedCornerShape(20.dp))
            .then(modifier),
    ) {
        Box(
            modifier = Modifier
                .size(80.dp)
                .clip(RoundedCornerShape(20.dp))
                .background(Color(0xFF0E3E3E)),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "$number",
                style = LocalAppTheme.typography.l24.copy(
                    fontSize = 40.sp
                ),
                color = Color.White
            )
        }

        Spacer(Modifier.width(10.dp))

        Column(
            modifier = Modifier
                .padding(vertical = 14.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = title,
                style = LocalAppTheme.typography.l15B,
                color = LocalAppTheme.colors.white
            )

            Spacer(Modifier.height(2.dp))

            Text(
                text = description,
                style = LocalAppTheme.typography.l10,
                color = LocalAppTheme.colors.white
            )
        }
    }
}