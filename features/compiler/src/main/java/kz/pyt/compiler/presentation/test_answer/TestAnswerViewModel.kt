package kz.mmq.test.presentation.test_answer

import kz.pyt.features.compiler.R
import kz.pyt.ui.base.navigation.NavigateTo
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState


data class TestAnswerState(
    val categoryName: String = "",
    val categoryIdShop: Int = 0,
    val description: String = "",
    val petImage: String = "",
    val total: Int
) : UiState

class TestAnswerViewModel(
    private val totalPoint: Int,
) : BaseViewModel<TestAnswerState>() {
    override fun createInitialState() = TestAnswerState(
        total = totalPoint
    )

}