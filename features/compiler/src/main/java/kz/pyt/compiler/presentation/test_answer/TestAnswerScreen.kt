package kz.mmq.test.presentation.test_answer

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.parcelize.Parcelize
import kz.pyt.ui.DestinationController
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.base.Destination
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.safeNavigationPadding
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CommonButton
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

@Parcelize
class TestAnswerDestination(
    private val totalPoint: Int,
) : Destination {
    @Composable
    override fun Content(controller: DestinationController) {
        val viewModel = getViewModel<TestAnswerViewModel> {
            parametersOf(totalPoint)
        }
        TestAnswerScreen(viewModel)
    }
}

@Composable
fun TestAnswerScreen(
    viewModel: TestAnswerViewModel
) {
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF0A4F4F))
            .safeStatusBarPadding(),
    ) {

        Spacer(modifier = Modifier.padding(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Your total score ...",
                style = LocalAppTheme.typography.l24,
                color = LocalAppTheme.colors.white,
                modifier = Modifier.padding(start = 16.dp)
            )

        }

        Spacer(modifier = Modifier.height(16.dp))

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    LocalAppTheme.colors.white,
                    RoundedCornerShape(topStart = 36.dp, topEnd = 36.dp)
                )
                .safeNavigationPadding(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = state.total.toString().plus("/3").uppercase(),
                style = LocalAppTheme.typography.l24.copy(
                    fontSize = 155.sp,
                    fontWeight = FontWeight.ExtraBold,
                    lineHeight = 180.sp
                ),
                color = Color(0xFF0A4F4F),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            )

            Spacer(modifier = Modifier.height(20.dp))

            CommonButton(
                text = "Go back",
                modifier = Modifier.padding(horizontal = 16.dp),
                buttonColors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF0A4F4F)
                ),
                onClick = {
                    viewModel.navigateBack()
                    viewModel.navigateBack()
                }
            )
        }
    }
}