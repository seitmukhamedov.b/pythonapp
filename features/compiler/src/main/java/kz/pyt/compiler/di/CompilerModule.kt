package kz.pyt.compiler.di

import kz.pyt.compiler.presentation.TestDelegateImpl
import kz.pyt.compiler.presentation.TestViewModel
import kz.mmq.test.presentation.test_answer.TestAnswerViewModel
import kz.pyt.compiler.presentation.test_questions.TestQuestionsViewModel
import kz.pyt.compiler.presentation.lesson.LessonViewModel
import kz.pyt.employee.checklist.CompilerContentDelegate
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val CompilerModule = module {
    factory<CompilerContentDelegate> { TestDelegateImpl }

    viewModelOf(::TestViewModel)
    viewModelOf(::TestQuestionsViewModel)
    viewModelOf(::TestAnswerViewModel)
    viewModelOf(::LessonViewModel)
}