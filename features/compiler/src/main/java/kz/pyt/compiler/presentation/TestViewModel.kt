package kz.pyt.compiler.presentation

import kz.pyt.compiler.presentation.test_questions.TestQuestionsDestination
import kz.pyt.compiler.presentation.lesson.LessonDestination
import kz.pyt.ui.base.navigation.NavigateTo
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.NoState

class TestViewModel : BaseViewModel<NoState>() {
    override fun createInitialState() = NoState

    fun onClickLesson(){
        navigate(NavigateTo(LessonDestination()))
    }

    fun onClickStart() {
        navigate(NavigateTo(TestQuestionsDestination()))
    }
}