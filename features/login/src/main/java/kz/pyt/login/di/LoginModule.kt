package kz.pyt.login.di

import kz.pyt.login.presentation.LoginViewModel
import kz.pyt.login.presentation.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val loginModule = module {
    viewModelOf(::LoginViewModel)
    viewModelOf(::RegistrationViewModel)
}
