package kz.pyt.login.presentation.registration

import android.net.Uri
import kz.pyt.store.UserDetail
import kz.pyt.store.UserStore
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState

data class RegistrationState(
    val school: String = "",
    val password: String = "",
    val selectedAvatar: Uri? = null,
    val firstName: String = "",
    val lastName: String = "",
    val phoneNumber: String = "",
    val phoneNumberTeacher: String = "",
    val classs: String = "",
    val address: String = "",
    val isLoading: Boolean = false,
) : UiState

class RegistrationViewModel(
    private val userStore: UserStore
) : BaseViewModel<RegistrationState>() {

    override fun createInitialState() = RegistrationState()

    fun onUpdateSchool(item: String) {
        setState { copy(school = item) }
    }

    fun onUpdatePassword(item: String) {
        setState { copy(password = item) }
    }

    fun onUpdateFirstName(item: String) {
        setState { copy(firstName = item) }
    }

    fun onUpdateLastName(item: String) {
        setState { copy(lastName = item) }
    }

    fun onUpdatePhoneNumber(item: String) {
        setState { copy(phoneNumber = item) }
    }

    fun onUpdatePhoneNumberTeacher(item: String) {
        setState { copy(phoneNumberTeacher = item) }
    }

    fun onUpdateClasss(str: String) {
        setState { copy(classs = str) }
    }

    fun onUpdateAddress(str: String) {
        setState { copy(address = str) }
    }

    fun setAvatar(uri: Uri) {
        setState {
            copy(
                selectedAvatar = uri
            )
        }
    }

    fun onClickProceed() {
        launch {
            setState {
                copy(
                    isLoading = true
                )
            }
            userStore.userDetails = UserDetail(
                id = 0,
                firstName = currentState.firstName,
                lastName = currentState.lastName,
                phoneNumber = currentState.phoneNumber,
                school = currentState.school,
                classs = currentState.classs,
                address = currentState.address,
                password = currentState.password,
                phoneNumberTeacher = currentState.phoneNumberTeacher
            )
            setState {
                copy(
                    isLoading = false
                )
            }

            navigateBack()
        }
    }
}