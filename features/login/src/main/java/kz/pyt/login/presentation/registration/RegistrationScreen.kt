package kz.pyt.login.presentation.registration

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import kotlinx.parcelize.Parcelize
import kz.pyt.ui.DestinationController
import kz.pyt.ui.R
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.base.Destination
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.safeNavigationPadding
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CommonButton
import kz.pyt.ui.widgets.CommonPasswordTextField
import kz.pyt.ui.widgets.CommonTextField
import org.koin.androidx.compose.getViewModel

@Parcelize
class RegistrationDestination : Destination {
    @Composable
    override fun Content(controller: DestinationController) {
        RegistrationScreen(controller)
    }

}

@Composable
fun RegistrationScreen(
    controller: DestinationController
) {

    val viewModel = getViewModel<RegistrationViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    ComposeEventHandler(
        event = viewModel.event,
    )

    ComposeEffectHandler(
        effect = viewModel.effect,
    )

    val singlePhotoPickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.PickVisualMedia(),
        onResult = { uri ->
            viewModel.setAvatar(uri ?: Uri.EMPTY)
        }
    )

    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .imePadding()
            .safeNavigationPadding(),
    ) {
        Box(
            modifier = Modifier
                .background(LocalAppTheme.colors.white)
                .fillMaxHeight(0.4f),
            contentAlignment = Alignment.Center,
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(R.drawable.ic_python),
                contentDescription = "",
                tint = Color.Unspecified,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(40.dp)
            )
        }

        Spacer(modifier = Modifier.height(16.dp))

        Column(
            modifier = Modifier
                .padding(horizontal = 24.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Text(
                text = stringResource(id = R.string.registration_type_choose_title),
                style = LocalAppTheme.typography.h2,
                color = LocalAppTheme.colors.primaryText,
            )

            Spacer(modifier = Modifier.height(24.dp))

            CommonTextField(
                value = state.firstName,
                placeholderText = "Аты",
                onUpdate = {
                    viewModel.onUpdateFirstName(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.lastName,
                placeholderText = "Тегі",
                onUpdate = {
                    viewModel.onUpdateLastName(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.phoneNumber,
                placeholderText = "Телефон нөмері (+77076665544)",
                onUpdate = {
                    viewModel.onUpdatePhoneNumber(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.phoneNumberTeacher,
                placeholderText = "Мұғалімнің телефон нөмері (77076665544)",
                onUpdate = {
                    viewModel.onUpdatePhoneNumberTeacher(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.school,
                placeholderText = "Мектебі",
                onUpdate = {
                    viewModel.onUpdateSchool(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.classs,
                placeholderText = "Сыныбы",
                onUpdate = {
                    viewModel.onUpdateClasss(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonTextField(
                value = state.address,
                placeholderText = "Мекен-жайы",
                onUpdate = {
                    viewModel.onUpdateAddress(it)
                },
            )

            Spacer(modifier = Modifier.height(8.dp))

            CommonPasswordTextField(
                value = state.password,
                onUpdate = {
                    viewModel.onUpdatePassword(it)
                },
            )

            Spacer(modifier = Modifier.height(24.dp))

            CommonButton(
                text = stringResource(id = R.string.registration_save),
                modifier = Modifier.fillMaxWidth(),
                buttonColors = ButtonDefaults.buttonColors(
                    backgroundColor = LocalAppTheme.colors.main,
                    disabledBackgroundColor = LocalAppTheme.colors.bgButtonSecondary,
                ),
                isLoading = state.isLoading,
                onClick = viewModel::onClickProceed,
            )

        }
    }
}