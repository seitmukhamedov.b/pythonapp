@file:OptIn(ExperimentalComposeUiApi::class)

package kz.pyt.login.presentation

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.autofill.AutofillType
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import kz.pyt.ui.DestinationController
import kz.pyt.ui.LocalDestinationController
import kz.pyt.ui.base.AcceptResult
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.collectInLaunchedEffectWithLifecycle
import kz.pyt.ui.ext.rememberKeyboardOpenState
import kz.pyt.ui.ext.safeNavigationPadding
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.screen.CommonDialogDestination
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.utils.connectNode
import kz.pyt.ui.utils.defaultFocusChangeAutoFill
import kz.pyt.ui.utils.rememberAutoFillRequestHandler
import kz.pyt.ui.widgets.CommonButton
import kz.pyt.ui.widgets.CommonPasswordTextField
import kz.pyt.ui.widgets.PhoneTextField
import org.koin.androidx.compose.getViewModel
import kz.pyt.ui.R as uiR

@Composable
fun LoginScreen(
    controller: DestinationController = LocalDestinationController,
    acceptResult: AcceptResult?
) {
    val viewModel = getViewModel<LoginViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    ComposeEventHandler(
        event = viewModel.event,
        controller = controller,
    )

    ComposeEffectHandler(
        effect = viewModel.effect,
    )

    val isKeyboardOpen by rememberKeyboardOpenState()

    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 24.dp)
            .imePadding()
            .verticalScroll(rememberScrollState())
            .safeStatusBarPadding()
            .safeNavigationPadding(),
    ) {

        Spacer(modifier = Modifier.height(90.dp))
        Text(
            text = stringResource(id = uiR.string.login_title),
            style = LocalAppTheme.typography.h2,
            color = LocalAppTheme.colors.primaryText,
        )

        Spacer(modifier = Modifier.height(24.dp))

        PhoneTextField(
            value = state.phone,
            countryCode = state.countryCode,
            onUpdate = {
                viewModel.onUpdatePhone(it)
            },
            onUpdateCountryCode = viewModel::onUpdateCountryCode,
        )

        Spacer(modifier = Modifier.height(8.dp))

        CommonPasswordTextField(
            value = state.password,
            onUpdate = {
                viewModel.onUpdatePassword(it)
            },
        )

        Spacer(modifier = Modifier.height(24.dp))

        CommonButton(
            text = stringResource(id = uiR.string.login_title),
            isEnabled = state.isFilled,
            isLoading = state.isLoading,
            modifier = Modifier.fillMaxWidth(),
            onClick = viewModel::onClickProceed
        )

        Spacer(modifier = Modifier.weight(1f))

        RegistrationButton(
            onClick = {
                viewModel.onClickRegistration(context)
            }
        )

        val spaceBottom by animateDpAsState(
            if (isKeyboardOpen) 0.dp else 36.dp
        )
        Spacer(modifier = Modifier.height(spaceBottom))
    }
}

@Composable
private fun RegistrationButton(onClick: () -> Unit) {
    TextButton(
        onClick = onClick,
        modifier = Modifier.fillMaxWidth(),
    ) {
        Row {
            Text(
                text = stringResource(id = uiR.string.login_no_account).uppercase(),
                style = LocalAppTheme.typography.l12.copy(
                    fontWeight = FontWeight.Bold
                ),
                color = LocalAppTheme.colors.accentText,
            )

            Text(
                text = " " + stringResource(id = uiR.string.common_registration).uppercase(),
                style = LocalAppTheme.typography.l12.copy(
                    fontWeight = FontWeight.Bold
                ),
                color = LocalAppTheme.colors.primaryText,
            )
        }
    }
}