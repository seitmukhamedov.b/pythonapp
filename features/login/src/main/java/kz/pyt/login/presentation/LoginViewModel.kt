package kz.pyt.login.presentation

import android.content.Context
import android.content.Intent
import android.net.Uri
import kotlinx.coroutines.flow.collectLatest
import kz.pyt.login.presentation.registration.RegistrationDestination
import kz.pyt.ui.R
import kz.pyt.ui.base.navigation.NavigateTo
import kz.pyt.ui.base.navigation.NavigateToMain
import kz.pyt.ui.base.navigation.ShowDialog
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState
import kz.pyt.ui.base.viewmodel.handleError
import kz.pyt.ui.ext.findActivity
import kz.pyt.ui.screen.CommonDialogDestination
import kz.pyt.ui.utils.VmRes
import kz.pyt.utils.Language
import kz.pyt.utils.LocaleUtil
import kz.pyt.utils.consts.CountryCode
import kz.pyt.utils.ext.getPhoneNumberWithCountryCode
import kz.pyt.utils.ext.onlyDigits
import kz.pyt.utils.outcome.Outcome
import kz.pyt.utils.outcome.doOnError
import kz.pyt.utils.outcome.doOnSuccess
import kz.pyt.store.UserStore
import kz.pyt.ui.base.effect.ToastEffect

data class State(
    val phone: String = "",
    val countryCode: CountryCode = CountryCode.KZ,
    val password: String = "",
    val isLoading: Boolean = false,
    val languages: List<Language>,
    val selectedLanguage: Language,
    val showOnboardingLanguage: Boolean = true,
) : UiState {
    val isFilled = phone.length == countryCode.numbersSize && password.isNotEmpty()
}

internal class LoginViewModel(
    private val userStore: UserStore,
) : BaseViewModel<State>() {

    override fun createInitialState() = State(
        languages = Language.entries.toList(),
        selectedLanguage = userStore.language,
    )

    init {
        userStore.accessToken = "access"
        userStore.refreshToken = "refrsh"
        navigate(NavigateToMain())
//        launch {
//            userStore.languageFlow.collectLatest {
//                setState {
//                    copy(selectedLanguage = it)
//                }
//            }
//        }
    }

    fun onUpdateCountryCode(countryCode: CountryCode) {
        setState {
            copy(countryCode = countryCode, phone = "")
        }
    }

    fun onUpdatePhone(phone: String) {
        setState { copy(phone = phone.onlyDigits()) }
    }

    fun onUpdatePassword(password: String) {
        setState { copy(password = password) }
    }


    fun onClickProceed() {
        launch {
            setState { copy(isLoading = true) }
            val phone = currentState.countryCode.code + currentState.phone
            val password = currentState.password.trim()
            if(phone == userStore.userDetails?.phoneNumber) {
                if(password == userStore.userDetails?.password) {
                    userStore.accessToken = "access"
                    userStore.refreshToken = "refrsh"
                    navigate(NavigateToMain())
                } else {
                    setState { copy(isLoading = false) }
                    setEffect { ToastEffect(VmRes.Str("Деректер дұрыс емес")) }
                }
            } else {
                setState { copy(isLoading = false) }
                setEffect { ToastEffect(VmRes.Str("Деректер дұрыс емес")) }
            }
        }
    }

    fun onClickRegistration(context: Context) {
        navigate(NavigateTo(RegistrationDestination()))
    }

}
