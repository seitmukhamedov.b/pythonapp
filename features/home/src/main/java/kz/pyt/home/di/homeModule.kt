package kz.pyt.home.di

import kz.pyt.employee.home.HomeContentDelegate
import kz.pyt.home.presentation.HomeContentDelegateImpl
import kz.pyt.home.presentation.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val homeModule = module {
    factory<HomeContentDelegate> { HomeContentDelegateImpl }

    viewModelOf(::HomeViewModel)
}