package kz.pyt.home.presentation

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import kz.pyt.employee.home.HomeContentDelegate
import kz.pyt.features.home.R
import kz.pyt.ui.DestinationController
import kz.pyt.ui.LocalDestinationController
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.ext.advanceShadow
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.ext.scaledClickable
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CenteredToolbar
import kz.pyt.ui.widgets.CommonDivider
import kz.pyt.ui.widgets.CommonVerticalDivider
import org.koin.androidx.compose.getViewModel

object HomeContentDelegateImpl : HomeContentDelegate {
    @Composable
    override fun Content() {
        HomeScreen()
    }
}

@Composable
fun HomeScreen(
    controller: DestinationController = LocalDestinationController
) {
    val viewModel = getViewModel<HomeViewModel>()
    val state = viewModel.uiState.collectAsStateWithLifecycle()

    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event, controller = controller)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF0A4F4F)),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .safeStatusBarPadding()
                .padding(horizontal = 38.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                modifier = Modifier,
            ) {
                Text(
                    text = "Hi, Welcome Back",
                    style = LocalAppTheme.typography.l20,
                    color = Color(0xFFDFF7E2)
                )

                Text(
                    text = "Good Afternoon",
                    style = LocalAppTheme.typography.l14M,
                    color = Color(0xFFDFF7E2)
                )
            }

            Icon(
                imageVector = ImageVector.vectorResource(kz.pyt.ui.R.drawable.icon_notification_30),
                contentDescription = null,
                tint = Color.Unspecified
            )

        }

        Spacer(modifier = Modifier.height(80.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 60.dp)
        ) {
            Column(
                modifier = Modifier
            ) {
                Text(
                    text = "Total Balance",
                    style = LocalAppTheme.typography.l12M,
                    color = Color(0xFFDFF7E2)
                )

                Spacer(modifier = Modifier.height(4.dp))

                Text(
                    text = "3 879 003 ₸",
                    style = LocalAppTheme.typography.l24,
                    color = Color(0xFFDFF7E2)
                )
            }

            Spacer(Modifier.width(22.dp))
            CommonVerticalDivider(
                modifier = Modifier.height(50.dp),
                color = Color(0xFFDFF7E2)
            )
            Spacer(Modifier.width(22.dp))

            Column(
                modifier = Modifier
            ) {
                Text(
                    text = "Total Expense",
                    style = LocalAppTheme.typography.l12M,
                    color = Color(0xFFDFF7E2)
                )

                Spacer(modifier = Modifier.height(4.dp))

                Text(
                    text = "-59 184 ₸",
                    style = LocalAppTheme.typography.l24,
                    color = Color(0xFF0068FF)
                )
            }
        }

        Spacer(modifier = Modifier.height(30.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(Color(0xFFF1FFF3), RoundedCornerShape(topEnd = 40.dp, topStart = 40.dp)),
            verticalArrangement = Arrangement.Center
        ) {
            Row(
                modifier = Modifier
                    .padding(horizontal = 36.dp, vertical = 30.dp)
                    .fillMaxWidth()
                    .background(Color(0xFF0A4F4F), RoundedCornerShape(30.dp)),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier
                        .padding(vertical = 23.dp, horizontal = 30.dp)
                ) {
                    Icon(
                        imageVector = ImageVector.vectorResource(kz.pyt.ui.R.drawable.ic_group_car_70),
                        contentDescription = null,
                        tint = Color.Unspecified
                    )

                    Spacer(modifier = Modifier.height(15.dp))

                    Text(
                        text = "Savings on goals",
                        style = LocalAppTheme.typography.l12M,
                        color = Color(0xFFDFF7E2)
                    )
                }

                CommonVerticalDivider(
                    modifier = Modifier
                        .height(100.dp),
                    color = Color(0xFFDFF7E2),
                    thickness = 2.dp
                )

                Column(
                    modifier = Modifier
                        .padding(vertical = 13.dp, horizontal = 30.dp)
                ) {
                    Row(
                        modifier = Modifier
                    ) {
                        Icon(
                            imageVector = ImageVector.vectorResource(kz.pyt.ui.R.drawable.ic_salary_30),
                            contentDescription = null,
                            tint = Color.Unspecified
                        )

                        Spacer(Modifier.width(5.dp))

                        Column(
                            modifier = Modifier
                        ) {
                            Text(
                                text = "Revenue Last Week",
                                style = LocalAppTheme.typography.l12,
                                color = Color(0xFFDFF7E2)
                            )

                            Spacer(modifier = Modifier.height(3.dp))

                            Text(
                                text = "1993,73 ₸",
                                style = LocalAppTheme.typography.l15,
                                color = Color(0xFFDFF7E2)
                            )
                        }
                    }

                    CommonDivider(
                        color = Color(0xFFDFF7E2),
                        thickness = 2.dp
                    )

                    Row(
                        modifier = Modifier
                    ) {
                        Icon(
                            imageVector = ImageVector.vectorResource(kz.pyt.ui.R.drawable.ic_food_25),
                            contentDescription = null,
                            tint = Color.Unspecified
                        )

                        Spacer(Modifier.width(5.dp))

                        Column(
                            modifier = Modifier
                        ) {
                            Text(
                                text = "Food Last Week",
                                style = LocalAppTheme.typography.l12,
                                color = Color(0xFFDFF7E2)
                            )

                            Spacer(modifier = Modifier.height(3.dp))

                            Text(
                                text = "-5000 ₸",
                                style = LocalAppTheme.typography.l15,
                                color = Color(0xFF0068FF)
                            )
                        }
                    }
                }
            }

            Row(
                modifier = Modifier
                    .padding(horizontal = 36.dp)
                    .padding(bottom = 24.dp)
                    .fillMaxWidth()
                    .background(Color(0xFFDFF7E2), RoundedCornerShape(22.dp)),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column (
                    modifier = Modifier
                        .padding(horizontal = 10.dp, vertical = 5.dp)
                        .background(if(state.value.isSelected != 1) Color(0xFFDFF7E2) else Color(0xFF00D09E), RoundedCornerShape(19.dp))
                        .scaledClickable {
                            viewModel.onClickCategory(1)
                        },
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Daily",
                        style = LocalAppTheme.typography.l15,
                        color = LocalAppTheme.colors.primaryText,
                        modifier = Modifier.padding(12.dp)
                    )
                }

                Spacer(modifier = Modifier.width(8.dp))

                Column(
                    modifier = Modifier
                        .padding(horizontal = 10.dp, vertical = 5.dp)
                        .background(if(state.value.isSelected != 2) Color(0xFFDFF7E2) else Color(0xFF00D09E), RoundedCornerShape(19.dp))
                        .scaledClickable {
                            viewModel.onClickCategory(2)
                        },
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Weekly",
                        style = LocalAppTheme.typography.l15,
                        color = LocalAppTheme.colors.primaryText,
                        modifier = Modifier.padding(12.dp)
                    )
                }

                Spacer(modifier = Modifier.width(8.dp))

                Column(
                    modifier = Modifier
                        .padding(horizontal = 10.dp, vertical = 5.dp)
                        .background(if(state.value.isSelected != 3) Color(0xFFDFF7E2) else Color(0xFF00D09E), RoundedCornerShape(19.dp))
                        .scaledClickable {
                            viewModel.onClickCategory(3)
                        },
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Monthly",
                        style = LocalAppTheme.typography.l15,
                        color = LocalAppTheme.colors.primaryText,
                        modifier = Modifier.padding(12.dp)
                    )
                }
            }

            SalaryComponent(
                icon = kz.pyt.ui.R.drawable.icon_salary_50,
                title = "Salary",
                time = state.value.time1,
                additionalText = state.value.text1,
                money = state.value.money1,
                moneyColor = LocalAppTheme.colors.primaryText
            )

            Spacer(Modifier.height(24.dp))

            SalaryComponent(
                icon = kz.pyt.ui.R.drawable.icon_groceries_50,
                title = "Groceries",
                time = state.value.time2,
                additionalText = state.value.text2,
                money = state.value.money2,
                moneyColor = Color(0xFF0068FF)
            )

            Spacer(Modifier.height(24.dp))

            SalaryComponent(
                icon = kz.pyt.ui.R.drawable.icon_rent_50,
                title = "Rent",
                time = state.value.time3,
                additionalText = state.value.text3,
                money = state.value.money3,
                moneyColor = Color(0xFF0068FF)
            )

        }
    }
}

@Composable
private fun SalaryComponent(
    @DrawableRes icon: Int,
    title: String,
    time: String,
    additionalText: String,
    money: String,
    moneyColor: Color,
) {
    Row(
        modifier = Modifier
            .padding(horizontal = 36.dp)
            .fillMaxWidth()
    ) {
        Icon(
            imageVector = ImageVector.vectorResource(icon),
            contentDescription = null,
            tint = Color.Unspecified
        )

        Spacer(Modifier.width(16.dp))

        Column(
            modifier = Modifier
        ) {
            Text(
                text = title,
                style = LocalAppTheme.typography.l15,
                color = LocalAppTheme.colors.primaryText
            )

            Text(
                text = time,
                style = LocalAppTheme.typography.l12B,
                color = Color(0xFF0068FF)
            )
        }

        Spacer(Modifier.width(16.dp))
        CommonVerticalDivider(
            modifier = Modifier.height(20.dp),
            color = Color(0xFF00D09E),
        )
        Spacer(Modifier.width(16.dp))

        Text(
            text = additionalText,
            style = LocalAppTheme.typography.l13,
            color = LocalAppTheme.colors.primaryText
        )

        Spacer(Modifier.width(16.dp))
        CommonVerticalDivider(
            modifier = Modifier.height(20.dp),
            color = Color(0xFF00D09E),
        )
        Spacer(Modifier.width(16.dp))

        Text(
            text = money,
            style = LocalAppTheme.typography.l15B,
            color = moneyColor
        )
    }
}