package kz.pyt.home.presentation

import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState

data class HomeState(
    val isSelected: Int = 1,
    val time1: String = "",
    val text1: String = "",
    val money1: String = "",

    val time2: String = "",
    val text2: String = "",
    val money2: String = "",

    val time3: String = "",
    val text3: String = "",
    val money3: String = "",
) : UiState

class HomeViewModel : BaseViewModel<HomeState>() {
    override fun createInitialState() = HomeState()

    init {
        onClickCategory(1)
    }

    fun onClickCategory(id: Int) {
        val (time1, text1, money1) = when(id) {
            3 -> Triple("18:27 - April 30", "Monthly", "200000 ₸")
            2 -> Triple("12:00 - June 14", "Monthly", "40000 ₸")
            else -> Triple("03:00 - May 14", "Monthly", "90000 ₸")
        }
        val (time2, text2, money2) = when(id) {
            3 -> Triple("17:00 - April 24", "Pantry", "-5000 ₸")
            2 -> Triple("15:54 - June 10", "Pantry", "-3200 ₸")
            else -> Triple("12:52 - May 5", "Pantry", "-10000 ₸")
        }
        val (time3, text3, money3) = when(id) {
            3 -> Triple("8:30 - April 15", "Rent", "-15 250 ₸")
            2 -> Triple("12:40 - June 10", "Rent", "-10 250 ₸")
            else -> Triple("03:00 - June 1", "Rent", "-20 500 ₸")
        }
        setState {
            copy(
                isSelected = id,
                time1 = time1,
                text1 = text1,
                money1 = money1,

                time2 = time2,
                text2 = text2,
                money2 = money2,

                time3 = time3,
                text3 = text3,
                money3 = money3,
            )
        }
    }
}