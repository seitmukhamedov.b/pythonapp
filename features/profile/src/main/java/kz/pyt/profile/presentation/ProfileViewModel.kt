package kz.pyt.profile.presentation

import kz.pyt.store.UserStore
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState

data class ProfileState(
    val firstName: String = ""
): UiState

class ProfileViewModel(
    val userStore: UserStore,
) : BaseViewModel<ProfileState>() {
    override fun createInitialState() = ProfileState()
}