package kz.pyt.profile.presentation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kz.pyt.employee.settings.EmployeeSettingsContentDelegate
import kz.pyt.ui.R
import kz.pyt.ui.DestinationController
import kz.pyt.ui.LocalDestinationController
import kz.pyt.ui.base.ComposeEffectHandler
import kz.pyt.ui.base.ComposeEventHandler
import kz.pyt.ui.ext.advanceShadow
import kz.pyt.ui.ext.collectAsStateWithLifecycle
import kz.pyt.ui.ext.safeNavigationPadding
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.CenteredToolbar
import kz.pyt.ui.widgets.CommonTextFieldWithTitle
import org.koin.androidx.compose.getViewModel

object EmployeeSettingsContentDelegateImpl : EmployeeSettingsContentDelegate {
    @Composable
    override fun Content() {
        ProfileScreen()
    }
}

@Composable
fun ProfileScreen(
    controller: DestinationController = LocalDestinationController
) {
    val viewModel = getViewModel<ProfileViewModel>()

    ComposeEffectHandler(effect = viewModel.effect)
    ComposeEventHandler(event = viewModel.event, controller = controller)

}