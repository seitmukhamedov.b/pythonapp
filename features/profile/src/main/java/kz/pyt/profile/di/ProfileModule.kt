package kz.pyt.profile.di

import kz.pyt.employee.home.HomeContentDelegate
import kz.pyt.employee.settings.EmployeeSettingsContentDelegate
import kz.pyt.profile.presentation.EmployeeSettingsContentDelegateImpl
import kz.pyt.profile.presentation.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val profileModule = module {
    factory<EmployeeSettingsContentDelegate> { EmployeeSettingsContentDelegateImpl }

    viewModelOf(::ProfileViewModel)
}