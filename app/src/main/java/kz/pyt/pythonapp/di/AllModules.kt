package kz.pyt.pythonapp.di

import kz.pyt.MainActivityIntentReceiverImpl
import kz.pyt.compiler.di.CompilerModule
import kz.pyt.employee.di.bottomNavModule
import kz.pyt.forefront_nav.MainActivityIntentReceiver
import kz.pyt.home.di.homeModule
import kz.pyt.local.di.localStorageModule
import kz.pyt.login.di.loginModule
import kz.pyt.main.start.startModule
import kz.pyt.network.di.networkModule
import kz.pyt.profile.di.profileModule
import kz.pyt.store.UserLogoutCleanUseCase
import kz.pyt.store.di.storeModule
import kz.pyt.ui.di.uiModule
import org.koin.core.KoinApplication
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module

val KoinApplication.allModules
    get() = modules(
        appModule,
        networkModule,
        storeModule,
        localStorageModule,
        startModule,
        uiModule,
        loginModule,
        bottomNavModule,
        homeModule,
        CompilerModule,
        profileModule,
    )

val appModule = module {
    factoryOf(::MainActivityIntentReceiverImpl) bind MainActivityIntentReceiver::class

    factory {
        UserLogoutCleanUseCase(
            userStore = get(),
        ) {
            listOf(

            )
        }
    }
}
