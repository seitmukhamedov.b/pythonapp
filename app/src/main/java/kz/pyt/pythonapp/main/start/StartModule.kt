package kz.pyt.main.start

import kz.pyt.main.MainNavigationViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val startModule = module {
    viewModelOf(::GlobalViewModel)
    viewModelOf(::MainNavigationViewModel)
}
