package kz.pyt.main.start

import androidx.activity.compose.BackHandler
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import dev.olshevski.navigation.reimagined.AnimatedNavHost
import dev.olshevski.navigation.reimagined.DialogNavHost
import dev.olshevski.navigation.reimagined.material.BottomSheetNavHost
import dev.olshevski.navigation.reimagined.material.BottomSheetPropertiesSpec
import dev.olshevski.navigation.reimagined.pop
import kz.pyt.ui.LocalDestinationControllerProvider
import kz.pyt.ui.LocalMainContentBlurProvider
import kz.pyt.ui.LocalMainContentOverlayProvider
import kz.pyt.ui.MainContentBlur
import kz.pyt.ui.MainContentOverlayProvider
import kz.pyt.ui.base.SheetDestination
import kz.pyt.ui.base.navigation.NavigationTransitionSpec
import kz.pyt.ui.ext.safeStatusBarPadding
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.snackbar.LocalSnackbarContentProvider
import kz.pyt.ui.widgets.snackbar.SnackbarContent
import kz.pyt.ui.widgets.snackbar.SnackbarContentProvider
import org.koin.androidx.compose.getViewModel

private val defaultBottomSheetPropertiesSpec = BottomSheetPropertiesSpec<SheetDestination> {
    it.sheetSpec
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
internal fun StartNavigationScreen(
    viewModel: GlobalViewModel = getViewModel()
) {
    val mainContentBlur = remember {
        MainContentBlur()
    }
    val snackbarContent = remember {
        SnackbarContentProvider()
    }
    val mainContentOverlayProvider = remember {
        MainContentOverlayProvider()
    }
    val lastDestination = viewModel.navController.backstack.entries.lastOrNull()?.destination
    CompositionLocalProvider(
        LocalDestinationControllerProvider provides viewModel,
        LocalMainContentBlurProvider provides mainContentBlur,
        LocalSnackbarContentProvider provides snackbarContent,
        LocalMainContentOverlayProvider provides mainContentOverlayProvider,
    ) {
        Box(
            modifier = Modifier
                .background(LocalAppTheme.colors.white)
                .blur(mainContentBlur.blurRadius)
        ) {

            AnimatedNavHost(
                controller = viewModel.navController,
                transitionSpec = NavigationTransitionSpec,
            ) {
                it.Content(viewModel)
            }

            BottomSheetNavHost(
                controller = viewModel.sheetNavController,
                sheetPropertiesSpec = defaultBottomSheetPropertiesSpec,
                sheetLayoutModifier = Modifier.safeStatusBarPadding(),
                scrimColor = Color.Black.copy(alpha = 0.7f),
                onDismissRequest = viewModel::onDismissSheet,
            ) { destination ->
                destination.Content(controller = viewModel)
            }

            DialogNavHost(
                controller = viewModel.dialogNavController
            ) {
                Dialog(
                    onDismissRequest = viewModel::onDismissDialog,
                    properties = DialogProperties(usePlatformDefaultWidth = false)
                ) {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center,
                    ) {
                        it.OpenAnimation(controller = viewModel)

                        Box(
                            modifier = Modifier
                                .align(Alignment.BottomCenter)
                                .imePadding()
                                .fillMaxWidth()
                        ) {
                            SnackbarContent(
                                state = snackbarContent.state,
                            )
                        }
                    }
                }
            }

            mainContentOverlayProvider.composable.value?.invoke()

            if (viewModel.dialogNavController.backstack.entries.isEmpty()) {
                Box(
                    modifier = Modifier
                        .align(Alignment.BottomCenter)
                        .imePadding()
                        .navigationBarsPadding()
                        .fillMaxWidth()
                ) {
                    SnackbarContent(
                        state = snackbarContent.state,
                        paddingBottom = if (viewModel.isLastMainDestination) 64.dp else 0.dp
                    )
                }
            }
        }
    }

    LaunchedEffect(lastDestination) {
        snackbarContent.setState(null)
    }

    val isDialogsEmpty = viewModel.dialogNavController.backstack.entries.isEmpty()
    val isSheetsEmpty = viewModel.sheetNavController.backstack.entries.isEmpty()

    LaunchedEffect(isDialogsEmpty) {
        mainContentBlur.setBlurRadius(
            if (isDialogsEmpty) {
                0.dp
            } else {
                viewModel.dialogNavController.backstack.entries.lastOrNull()?.destination?.blurRadius
                    ?: 3.dp
            }
        )
    }

    BackHandler(
        enabled = viewModel.navController.backstack.entries.size > 1 || !isDialogsEmpty || !isSheetsEmpty
    ) {
        val hasDialog = viewModel.dialogNavController.backstack.entries.isNotEmpty()
        val hasSheet = viewModel.sheetNavController.backstack.entries.isNotEmpty()
        val screensCanBeBacked = viewModel.navController.backstack.entries.size > 1
        when {
            hasDialog -> {
                if (viewModel.dialogNavController.backstack.entries.last().destination.onNavigateBack()) {
                    viewModel.onDismissDialog()
                }
            }

            hasSheet -> {
                if (viewModel.sheetNavController.backstack.entries.last().destination.onNavigateBack()) {
                    viewModel.onDismissSheet()
                }
            }

            screensCanBeBacked -> {
                if (viewModel.navController.backstack.entries.last().destination.onNavigateBack()) {
                    viewModel.navController.pop()
                }
            }
        }
    }
}
