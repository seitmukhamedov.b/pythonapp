package kz.pyt.main

import kz.pyt.store.UserStore
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.NoState
import kz.pyt.ui.base.viewmodel.UiEvent

object ShowGambyDialog : UiEvent

class MainNavigationViewModel(
    private val userStore: UserStore,
) : BaseViewModel<NoState>() {

    override fun createInitialState() = NoState


}
