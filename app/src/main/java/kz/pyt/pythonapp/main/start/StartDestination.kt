package kz.pyt.main.start

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import kotlinx.parcelize.Parcelize
import kz.pyt.ui.DestinationController
import kz.pyt.ui.base.AcceptResult
import kz.pyt.ui.base.Destination
import kz.pyt.forefront_nav.ForefrontDestinationsDelegate
import kz.pyt.login.presentation.LoginScreen
import kz.pyt.main.LoadingUserDetailsScreen
import kz.pyt.main.MainNavigationScreen
import org.koin.compose.koinInject
import org.koin.core.component.KoinComponent
import org.koin.core.qualifier.named

@Parcelize
internal sealed interface StartDestination : Destination {

    @Immutable
    @Parcelize
    object Login : StartDestination, AcceptResult by AcceptResult.Base() {
        override val isLogin: Boolean
            get() = true

        @Composable
        override fun Content(controller: DestinationController) {
            LoginScreen(controller, this)
        }
    }

    @Immutable
    @Parcelize
    class Main : StartDestination,
        AcceptResult by AcceptResult.Base(),
        KoinComponent {

        override val isMain: Boolean
            get() = true

        @Composable
        override fun Content(controller: DestinationController) {
            val delegate = koinInject<ForefrontDestinationsDelegate>()
            MainNavigationScreen(
                forefrontDestinationsDelegate = delegate,
                acceptResult = this@Main,
            )
        }
    }

    @Immutable
    @Parcelize
    object LoadingUserDetails : StartDestination {

        @Composable
        override fun Content(controller: DestinationController) {
            LoadingUserDetailsScreen(controller)
        }
    }
}