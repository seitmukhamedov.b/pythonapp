package kz.pyt.main

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kz.pyt.ui.DestinationController
import kz.pyt.ui.LocalDestinationController
import kz.pyt.ui.widgets.LoadingProgress

@Composable
fun LoadingUserDetailsScreen(
    destinationController: DestinationController = LocalDestinationController,
) {
    val isFetched = true
    LaunchedEffect(isFetched) {
        if (isFetched) {
            destinationController.navigateToMain(replace = true)
        }
    }
    Box(modifier = Modifier.fillMaxSize()) {
        LoadingProgress(
            modifier = Modifier.align(Alignment.Center),
            isVisible = !isFetched,
        )
    }
}
