package kz.pyt

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.LaunchedEffect
import kz.pyt.main.start.GlobalViewModel
import kz.pyt.main.start.StartNavigationScreen
import kz.pyt.ui.theme.AppTheme
import kz.pyt.utils.ext.checkIsNeedLocalizing
import kz.pyt.utils.ext.getLocale
import kz.pyt.utils.ext.setLocale
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {
    private val pushNotificationPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { _ -> }

    private val viewModel by viewModel<GlobalViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {

        enableEdgeToEdge(
            statusBarStyle = SystemBarStyle.light(Color.TRANSPARENT, Color.TRANSPARENT),
            navigationBarStyle = SystemBarStyle.light(Color.TRANSPARENT, Color.TRANSPARENT),
        )

        super.onCreate(savedInstanceState)

        val languageCode = viewModel.userStore.language.code
        if (checkIsNeedLocalizing(languageCode)) {
            setLocale(languageCode)
            recreate()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            pushNotificationPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)
        }
        setContent {
            val locale = getLocale()
            LaunchedEffect(locale) {
                viewModel.setLocaleChanged(locale)
            }
            AppTheme(
                currentLocale = locale
            ) {
                StartNavigationScreen(viewModel)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }
}
