package kz.pyt.ui.base.effect

import android.content.Context
import android.widget.Toast
import kz.pyt.ui.R
import kz.pyt.ui.base.viewmodel.UiEffect
import kz.pyt.ui.utils.VmRes
import kz.pyt.ui.widgets.snackbar.SnackbarContentProvider
import kz.pyt.ui.widgets.snackbar.SnackbarContentState

open class ToastEffect(
    private val message: VmRes<CharSequence>,
): UiEffect {

    fun show(context: Context) {
        val message = message.get(context)
        if (message.isEmpty()) return
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}

open class SnackbarEffect(
    private val state: SnackbarContentState
): UiEffect {

    fun showSnackbar(
        snackbarContentProvider: SnackbarContentProvider,
    ) {
        snackbarContentProvider.setState(state)
    }
}

class ErrorEffect(
    message: VmRes<CharSequence>,
): SnackbarEffect(SnackbarContentState(text = message, isSuccess = false))

class SuccessEffect(
    message: VmRes<CharSequence>? = null,
): SnackbarEffect(
    SnackbarContentState(
        text = message ?: VmRes.Str(""),
        isSuccess = true
    )
)
