package kz.pyt.ui.widgets

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kz.pyt.ui.theme.LocalAppTheme

@Composable
fun CommonTextFieldWithTitle(
    modifier: Modifier = Modifier,
    title: String,
    value: String,
    onUpdate: (String) -> Unit,
    maxLines: Int = 1,
    isEnabled: Boolean = true,
    hasError: Boolean = false,
    errorMessage: String? = null,
    placeholder: String = "",
    readOnly: Boolean = false,
    isMultiline: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    minHeight: Dp = 48.dp,
    maxHeight: Dp = Dp.Unspecified,
    spacing: Dp = 4.dp,
    titleTrailingContent: (@Composable RowScope.() -> Unit)? = null,
    onClick: (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
) {
    CommonFieldWithTitle(
        modifier = modifier,
        title = title,
        spacing = spacing,
        trailingContent = titleTrailingContent
    ) {
        CommonTextField(
            value = value,
            onUpdate = onUpdate,
            maxLines = maxLines,
            hasError = hasError,
            readOnly = readOnly,
            isMultiline = isMultiline,
            errorMessage = errorMessage,
            placeholderText = placeholder,
            keyboardOptions = keyboardOptions,
            isEnabled = isEnabled,
            onClick = onClick,
            trailingIcon = trailingIcon,
            minHeight = minHeight,
            maxHeight = maxHeight,
        )
    }
}

@Composable
fun CommonPasswordFieldWithTitle(
    modifier: Modifier = Modifier,
    title: String,
    value: String,
    isEnabled: Boolean = true,
    hasError: Boolean = false,
    errorMessage: String? = null,
    placeholder: String = "",
    spacing: Dp = 4.dp,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    onUpdate: (String) -> Unit
) {
    CommonFieldWithTitle(
        modifier = modifier,
        title = title,
        spacing = spacing,
    ) {
        CommonPasswordTextField(
            value = value,
            onUpdate = onUpdate,
            hasError = hasError,
            errorMessage = errorMessage,
            placeholderText = placeholder,
            keyboardOptions = keyboardOptions,
            isEnabled = isEnabled,
        )
    }
}

@Composable
fun CommonFieldWithTitle(
    modifier: Modifier = Modifier,
    title: String,
    spacing: Dp = 4.dp,
    trailingContent: (@Composable RowScope.() -> Unit)? = null,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(spacing)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = title.uppercase(),
                style = LocalAppTheme.typography.l12,
                color = LocalAppTheme.colors.accentText,
                modifier = Modifier.weight(1f),
            )
            trailingContent?.invoke(this)
        }

        content()
    }
}