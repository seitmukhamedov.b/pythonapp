package kz.pyt.ui.utils

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.Spanned
import android.text.style.AbsoluteSizeSpan
import android.text.style.BulletSpan
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StrikethroughSpan
import android.text.style.StyleSpan
import android.text.style.SubscriptSpan
import android.text.style.SuperscriptSpan
import android.text.style.TypefaceSpan
import android.text.style.UnderlineSpan
import androidx.annotation.ArrayRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.BaselineShift
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

/**
 * Interface for get context depending value.
 *
 * Ex: If you use the application context for getColor and after the user changes the color, you may have problems with the color not changing,
 * to fix this you need to use this interface with the inherited class and get the value from the activity, fragment or view.
 */
@Stable
sealed interface VmRes<T> {

    /**
     * Get a value from inherited class.
     *
     * @param context pass the context from the fragment, activity or view to get the value depending on the configuration change.
     * @return value
     */
    fun get(context: Context): T

    /**
     * Returns a string representation of the object.
     *
     * @param context pass the context from the fragment, activity or view to get the value depending on the configuration change.
     * @return string value. Ex: if need convert CharSequence to String instead use .get(context).toString() you can .toString(context) or implement self logic for inherited class
     */
    fun toString(context: Context): String {
        return get(context).toString()
    }

    @Composable
    fun get(): T {
        val context = LocalContext.current
        return remember(this) {
            get(context)
        }
    }

    interface Parcelable<T> : VmRes<T>, android.os.Parcelable

    interface Compose<T> : VmRes<T> {
        override fun get(context: Context): T {
            throw NotImplementedError("Not Implemented")
        }
    }

    @Parcelize
    class StrRes(
        @StringRes private val value: Int,
        private vararg val args: @RawValue Any? = arrayOf(),
    ) : Parcelable<CharSequence> {
        override fun get(context: Context): CharSequence {
            return if (args.isNotEmpty()) {
                context.getString(value,
                    *args.map { if (it is VmRes<*>) it.get(context) else it }.toTypedArray())
            } else {
                context.getString(value)
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is StrRes) return false

            if (value != other.value) return false
            if (!args.contentEquals(other.args)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = value
            result = 31 * result + args.contentHashCode()
            return result
        }
    }

    @Parcelize
    class StrQuantityRes(
        @PluralsRes private val value: Int,
        private val quantity: Int,
        private vararg val args: @RawValue Any? = arrayOf(),
    ) : Parcelable<CharSequence> {
        override fun get(context: Context): CharSequence {
            return if (args.isNotEmpty()) {
                context.resources.getQuantityString(
                    value,
                    quantity,
                    *args.map { if (it is VmRes<*>) it.get(context) else it }.toTypedArray(),
                )
            } else {
                context.resources.getQuantityString(value, quantity, quantity)
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is StrQuantityRes) return false

            if (value != other.value) return false
            if (quantity != other.quantity) return false
            if (!args.contentEquals(other.args)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = value
            result = 31 * result + quantity
            result = 31 * result + args.contentHashCode()
            return result
        }
    }

    @Parcelize
    class StrArrayRes(
        @ArrayRes private val value: Int,
    ) : Parcelable<Array<String>> {
        override fun get(context: Context): Array<String> {
            return context.resources.getStringArray(value)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is StrArrayRes) return false

            if (value != other.value) return false

            return true
        }

        override fun hashCode(): Int {
            return value
        }
    }

    @Parcelize
    class Str(
        private val value: CharSequence,
        private vararg val args: @RawValue Any? = arrayOf(),
    ) : Parcelable<CharSequence> {
        override fun get(context: Context): CharSequence {
            return if (args.isNotEmpty()) {
                value.toString().format(*args.map { if (it is VmRes<*>) it.get(context) else it }
                    .toTypedArray())
            } else {
                value
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Str) return false

            if (value != other.value) return false
            if (!args.contentEquals(other.args)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = value.hashCode()
            result = 31 * result + args.contentHashCode()
            return result
        }
    }

    data class JoinedStr(
        private val values: List<VmRes<CharSequence>>,
        private val separator: CharSequence = "",
    ) : VmRes<CharSequence> {
        override fun get(context: Context): CharSequence {
            return values.joinToString(separator = separator) {
                it.get(context)
            }
        }
    }

    @Parcelize
    class ColorRes(@androidx.annotation.ColorRes private val value: Int) : Parcelable<Int> {
        override fun get(context: Context): Int {
            return ContextCompat.getColor(context, value)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is ColorRes) return false

            if (value != other.value) return false

            return true
        }

        override fun hashCode(): Int {
            return value
        }
    }

    @Parcelize
    class DrawableRes(@androidx.annotation.DrawableRes private val value: Int) :
        Parcelable<Drawable?> {
        override fun get(context: Context): Drawable? {
            return ContextCompat.getDrawable(context, value)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DrawableRes) return false

            if (value != other.value) return false

            return true
        }

        override fun hashCode(): Int {
            return value
        }
    }

    class Callback<R>(val block: (Context) -> R) : VmRes<R> {
        override fun get(context: Context): R {
            return block.invoke(context)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Callback<*>) return false

            if (block != other.block) return false

            return true
        }

        override fun hashCode(): Int {
            return block.hashCode()
        }
    }

    @Parcelize
    class Span(private vararg val values: Settings) : Parcelable<CharSequence> {

        @Parcelize
        class Settings(
            vararg val text: Parcelable<CharSequence>,
            val color: ColorRes? = null,
            val isBold: Boolean = false,
        ) : android.os.Parcelable {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (other !is Settings) return false

                if (!text.contentEquals(other.text)) return false
                if (color != other.color) return false
                if (isBold != other.isBold) return false

                return true
            }

            override fun hashCode(): Int {
                var result = text.contentHashCode()
                result = 31 * result + (color?.hashCode() ?: 0)
                result = 31 * result + isBold.hashCode()
                return result
            }
        }

        override fun get(context: Context): CharSequence {
            return buildSpannedString {
                values.forEach { settings ->
                    val spans = mutableListOf<Any>().apply {
                        if (settings.color != null) {
                            add(ForegroundColorSpan(settings.color.get(context)))
                        }
                        if (settings.isBold) {
                            add(StyleSpan(Typeface.BOLD))
                        }
                    }
                    inSpans(*spans.toTypedArray()) {
                        settings.text.forEach {
                            append(it.get(context))
                        }
                    }
                }
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Span) return false

            if (!values.contentEquals(other.values)) return false

            return true
        }

        override fun hashCode(): Int {
            return values.contentHashCode()
        }
    }

    @Parcelize
    class Operation(
        private val first: Parcelable<CharSequence>,
        private val second: Parcelable<CharSequence>,
        private val type: Type,
    ) : Parcelable<CharSequence> {

        enum class Type {
            SUBSTRING_BEFORE_LAST,
            SUBSTRING_AFTER
        }

        override fun get(context: Context): CharSequence {
            return when (type) {
                Type.SUBSTRING_BEFORE_LAST -> first.toString(context)
                    .substringBeforeLast(second.toString(context))
                Type.SUBSTRING_AFTER -> first.toString(context)
                    .substringAfter(second.toString(context))
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Operation) return false

            if (first != other.first) return false
            if (second != other.second) return false
            if (type != other.type) return false

            return true
        }

        override fun hashCode(): Int {
            var result = first.hashCode()
            result = 31 * result + second.hashCode()
            result = 31 * result + type.hashCode()
            return result
        }
    }
}

fun CharSequence.toVmResStr(): VmRes.Parcelable<CharSequence> {
    return VmRes.Str(this)
}

fun Int.toVmResStr(): VmRes.Parcelable<CharSequence> {
    return VmRes.StrRes(this)
}

operator fun VmRes.Parcelable<CharSequence>.plus(s: VmRes.Parcelable<CharSequence>) =
    VmRes.Span(VmRes.Span.Settings(this, s))

fun VmRes.Parcelable<CharSequence>.substringBeforeLast(s: VmRes.Parcelable<CharSequence>) =
    VmRes.Operation(this, s, VmRes.Operation.Type.SUBSTRING_BEFORE_LAST)

fun VmRes.Parcelable<CharSequence>.substringAfter(s: VmRes.Parcelable<CharSequence>) =
    VmRes.Operation(this, s, VmRes.Operation.Type.SUBSTRING_AFTER)

@Composable
fun <T> VmRes<T>.toStringCompose(): String {
    val context = LocalContext.current
    return remember(this) {
        toString(context)
    }
}

@Composable
fun VmRes<CharSequence>.rememberAsAnnotatedString(): AnnotatedString {
    val text = get()
    return remember(text) {
        text.toAnnotatedString()
    }
}

fun CharSequence.toAnnotatedString(): AnnotatedString {
    if (this is AnnotatedString) {
        return this
    }

    val text = this

    return if (text is Spanned) {
        buildAnnotatedString {
            append((text.toString()))
            text.getSpans(0, text.length, Any::class.java).forEach {
                val start = text.getSpanStart(it)
                val end = text.getSpanEnd(it)
                when (it) {
                    is StyleSpan -> when (it.style) {
                        Typeface.NORMAL -> addStyle(
                            SpanStyle(
                                fontWeight = FontWeight.Normal,
                                fontStyle = FontStyle.Normal
                            ),
                            start,
                            end
                        )
                        Typeface.BOLD -> addStyle(
                            SpanStyle(
                                fontWeight = FontWeight.Bold,
                                fontStyle = FontStyle.Normal
                            ),
                            start,
                            end
                        )
                        Typeface.ITALIC -> addStyle(
                            SpanStyle(
                                fontWeight = FontWeight.Normal,
                                fontStyle = FontStyle.Italic
                            ),
                            start,
                            end
                        )
                        Typeface.BOLD_ITALIC -> addStyle(
                            SpanStyle(
                                fontWeight = FontWeight.Bold,
                                fontStyle = FontStyle.Italic
                            ),
                            start,
                            end
                        )
                    }
                    is TypefaceSpan -> addStyle(
                        SpanStyle(
                            fontFamily = when (it.family) {
                                FontFamily.SansSerif.name -> FontFamily.SansSerif
                                FontFamily.Serif.name -> FontFamily.Serif
                                FontFamily.Monospace.name -> FontFamily.Monospace
                                FontFamily.Cursive.name -> FontFamily.Cursive
                                else -> FontFamily.Default
                            }
                        ),
                        start,
                        end
                    )
                    is BulletSpan -> {
                        addStyle(SpanStyle(), start, end)
                    }
                    is AbsoluteSizeSpan -> addStyle(
                        SpanStyle(fontSize = if (it.dip) it.size.sp else it.size.sp),
                        start,
                        end
                    )
                    is RelativeSizeSpan -> addStyle(
                        SpanStyle(fontSize = it.sizeChange.em),
                        start,
                        end
                    )
                    is StrikethroughSpan -> addStyle(
                        SpanStyle(textDecoration = TextDecoration.LineThrough),
                        start,
                        end
                    )
                    is UnderlineSpan -> addStyle(
                        SpanStyle(textDecoration = TextDecoration.Underline),
                        start,
                        end
                    )
                    is SuperscriptSpan -> addStyle(
                        SpanStyle(baselineShift = BaselineShift.Superscript),
                        start,
                        end
                    )
                    is SubscriptSpan -> addStyle(
                        SpanStyle(baselineShift = BaselineShift.Subscript),
                        start,
                        end
                    )
                    is ForegroundColorSpan -> addStyle(
                        SpanStyle(color = Color(it.foregroundColor)),
                        start,
                        end
                    )
                    else -> addStyle(SpanStyle(), start, end)
                }
            }
        }
    } else {
        AnnotatedString(text.toString())
    }
}
