//package kz.pyt.ui.widgets.checkables.radio
//
//import androidx.compose.animation.animateColorAsState
//import androidx.compose.animation.core.animateDpAsState
//import androidx.compose.foundation.background
//import androidx.compose.foundation.border
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.interaction.MutableInteractionSource
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.size
//import androidx.compose.foundation.shape.CircleShape
//import androidx.compose.material.ripple.rememberRipple
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.clip
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.res.colorResource
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.tooling.preview.PreviewParameter
//import androidx.compose.ui.unit.dp
//import com.snoonu.core.ui_kit.R
//import com.snoonu.core.ui_kit.base.checkable.CheckablePreviewParameterProvider
//import com.snoonu.core.ui_kit.base.checkable.CheckableState
//import com.snoonu.core.ui_kit.base.checkable.updateWithOverlayColor
//
//private val radioCheckedBorderStrokeDp = 8.dp
//private val radioUncheckedBorderStrokeDp = 2.dp
//private val radioSize = 24.dp
//private const val RADIO_OVERLAY_DISABLED_ALPHA = 0.74f
//
//@Composable
//fun RadioComponent(
//    state: CheckableState,
//    onCheckedChange: (Boolean) -> Unit,
//    modifier: Modifier = Modifier,
//) {
//    val enabledRadioColor = colorResource(R.color.color_control_control_primary)
//    val disabledBgColor = colorResource(R.color.color_background_background_secondary)
//    val backgroundColor = animateColorAsState(
//        targetValue = if (!state.isEnabled && !state.isChecked) {
//            disabledBgColor
//        } else {
//            Color.Transparent
//        },
//        label = "RadioComponent_backgroundColor",
//    )
//
//    val borderWidth = animateDpAsState(
//        targetValue = if (state.isChecked) {
//            radioCheckedBorderStrokeDp
//        } else {
//            radioUncheckedBorderStrokeDp
//        },
//        label = "RadioComponent_borderWidth",
//    )
//
//    val compositeBorderColor = if (state.isChecked) {
//        updateWithOverlayColor(
//            enabledColor = enabledRadioColor,
//            overlayColor = colorResource(R.color.color_constant_constant_white).copy(
//                alpha = RADIO_OVERLAY_DISABLED_ALPHA
//            ),
//            isEnabled = state.isEnabled,
//        )
//    } else {
//        updateWithOverlayColor(
//            enabledColor = colorResource(R.color.color_border_divider_border_divider_secondary),
//            overlayColor = Color.Transparent,
//            isEnabled = state.isEnabled
//        )
//    }
//
//    val borderColor = animateColorAsState(
//        targetValue = compositeBorderColor,
//        label = "RadioComponent_borderColor"
//    )
//
//    Box(
//        modifier = modifier
//            .clip(CircleShape)
//            .clickable(
//                interactionSource = remember {
//                    MutableInteractionSource()
//                },
//                indication = rememberRipple(
//                    bounded = false,
//                    radius = radioSize,
//                    color = enabledRadioColor,
//                ),
//                enabled = state.isEnabled,
//                onClick = {
//                    onCheckedChange.invoke(!state.isChecked)
//                }
//            )
//            .size(radioSize)
//            .background(backgroundColor.value)
//            .border(
//                width = borderWidth.value,
//                color = borderColor.value,
//                shape = CircleShape,
//            )
//    )
//}
//
//@Composable
//@Preview
//private fun RadioComponent_Preview(
//    @PreviewParameter(CheckablePreviewParameterProvider::class) state: CheckableState
//) {
//    var currentState by remember {
//        mutableStateOf(state)
//    }
//    RadioComponent(
//        state = currentState,
//        onCheckedChange = {
//            currentState = state.copy(
//                isChecked = it
//            )
//        },
//    )
//}
