//package kz.pyt.ui.widgets.checkables.checkbox
//
//import androidx.compose.animation.AnimatedVisibility
//import androidx.compose.animation.animateColorAsState
//import androidx.compose.animation.core.animateDpAsState
//import androidx.compose.animation.fadeIn
//import androidx.compose.animation.fadeOut
//import androidx.compose.animation.scaleIn
//import androidx.compose.animation.scaleOut
//import androidx.compose.foundation.background
//import androidx.compose.foundation.border
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.size
//import androidx.compose.foundation.shape.RoundedCornerShape
//import androidx.compose.material.Icon
//import androidx.compose.material.IconButton
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.clip
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.res.colorResource
//import androidx.compose.ui.res.painterResource
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.tooling.preview.PreviewParameter
//import androidx.compose.ui.unit.dp
//import kz.pyt.ui.theme.LocalAppTheme
//import kz.pyt.ui.widgets.checkables.CheckableState
//import kz.pyt.ui.widgets.checkables.updateWithOverlayColor
//
//private val checkboxCheckedBorderStrokeDp = 0.dp
//private val checkboxUncheckedBorderStrokeDp = 2.dp
//private val checkboxSize = 24.dp
//private val checkboxIconSize = 16.dp
//private const val CHECKBOX_OVERLAY_DISABLED_ALPHA = 0.74f
//
//data class CheckboxColors(
//    val primary
//)
//
//@Composable
//fun CheckboxComponent(
//    state: CheckableState,
//    onCheckedChange: (Boolean) -> Unit,
//    modifier: Modifier = Modifier,
//) {
//    val checkboxShapeRadius = 8.dp
//    val checkboxShape = RoundedCornerShape(checkboxShapeRadius)
//    val border = animateDpAsState(
//        targetValue = if (state.isChecked) {
//            checkboxCheckedBorderStrokeDp
//        } else {
//            checkboxUncheckedBorderStrokeDp
//        },
//        label = "CheckboxComponent_borderSize",
//    )
//    val borderUncheckedColor = colorResource(R.color.color_border_divider_border_divider_primary)
//    val borderCheckedColor = Color.Transparent
//    val borderColor = animateColorAsState(
//        targetValue = if (state.isChecked) {
//            borderCheckedColor
//        } else {
//            borderUncheckedColor
//        },
//        label = "CheckboxComponent_borderColor"
//    )
//    val enabledCheckboxColor = colorResource(R.color.color_control_control_primary)
//
//    val compositeBgColor = if (state.isChecked) {
//        updateWithOverlayColor(
//            enabledColor = enabledCheckboxColor,
//            overlayColor = LocalAppTheme.colors.white.copy(
//                alpha = CHECKBOX_OVERLAY_DISABLED_ALPHA
//            ),
//            isEnabled = state.isEnabled,
//        )
//    } else {
//        updateWithOverlayColor(
//            enabledColor = Color.Transparent,
//            overlayColor = colorResource(R.color.color_background_background_secondary),
//            isEnabled = state.isEnabled
//        )
//    }
//
//    val backgroundColor = animateColorAsState(
//        targetValue = compositeBgColor,
//        label = "CheckboxComponent_backgroundColor",
//    )
//
//    IconButton(
//        onClick = {
//            onCheckedChange.invoke(!state.isChecked)
//        },
//        enabled = state.isEnabled,
//        modifier = modifier.size(checkboxSize),
//    ) {
//        Box(
//            modifier = Modifier
//                .size(checkboxSize)
//                .clip(checkboxShape)
//                .background(backgroundColor.value)
//                .border(
//                    width = border.value,
//                    color = borderColor.value,
//                    shape = checkboxShape,
//                ),
//            contentAlignment = Alignment.Center,
//        ) {
//            AnimatedVisibility(
//                visible = state.isChecked,
//                enter = fadeIn() + scaleIn(),
//                exit = scaleOut() + fadeOut(),
//            ) {
//                Icon(
//                    painter = painterResource(R.drawable.ic_checkbox_component_16),
//                    contentDescription = null,
//                    tint = colorResource(R.color.color_icon_icon_invert),
//                    modifier = Modifier.size(checkboxIconSize)
//                )
//            }
//        }
//    }
//}
//
//@Composable
//@Preview
//private fun CheckboxComponent_Preview(
//    @PreviewParameter(CheckablePreviewParameterProvider::class) state: CheckableState
//) {
//    var currentState by remember {
//        mutableStateOf(state)
//    }
//    CheckboxComponent(
//        state = currentState,
//        onCheckedChange = {
//            currentState = state.copy(
//                isChecked = it
//            )
//        },
//    )
//}
