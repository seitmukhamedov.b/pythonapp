package kz.pyt.ui.utils

import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.grid.LazyGridItemScope
import androidx.compose.foundation.lazy.grid.LazyGridScope
import androidx.compose.runtime.Composable
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemContentType
import androidx.paging.compose.itemKey

fun <T: Any> LazyListScope.pagingItems(
    items: LazyPagingItems<T>,
    key: ((item: @JvmSuppressWildcards T) -> Any)? = null,
    contentType: ((item: @JvmSuppressWildcards T) -> Any)? = null,
    content: @Composable LazyItemScope.(T?) -> Unit
) {
    items(
        count = items.itemCount,
        key = items.itemKey(key),
        contentType = items.itemContentType(contentType)
    ) { index ->
        val item = items[index]
        content(item)
    }
}

fun <T: Any> LazyGridScope.pagingItems(
    items: LazyPagingItems<T>,
    key: ((item: @JvmSuppressWildcards T) -> Any)? = null,
    contentType: ((item: @JvmSuppressWildcards T) -> Any)? = null,
    content: @Composable LazyGridItemScope.(T?) -> Unit
) {
    items(
        count = items.itemCount,
        key = items.itemKey(key),
        contentType = items.itemContentType(contentType)
    ) { index ->
        val item = items[index]
        content(item)
    }
}
