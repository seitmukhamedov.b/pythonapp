package kz.pyt.ui.widgets.snackbar

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.State
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.utils.VmRes
import kz.pyt.ui.utils.rememberAsAnnotatedString

@Stable
data class SnackbarContentState(
    val text: VmRes<CharSequence>,
    val isSuccess: Boolean,
    val duration: Long = 3000L,
)

class SnackbarContentProvider {
    val state = mutableStateOf<SnackbarContentState?>(null)
    private val coroutineScope = CoroutineScope(Dispatchers.Default)

    fun setState(value: SnackbarContentState?) {
        state.value = value
        if (value != null) {
            coroutineScope.launch {
                delay(value.duration)
                state.value = null
            }
        }
    }
}

val LocalSnackbarContentProvider = compositionLocalOf { SnackbarContentProvider() }

@Composable
fun SnackbarContent(
    state: State<SnackbarContentState?>,
    paddingBottom: Dp = 0.dp
) {
    val acutalPaddingBottom = animateDpAsState(targetValue = paddingBottom, label = "")
    AnimatedVisibility(
        visible = state.value != null,
        modifier = Modifier.fillMaxWidth(),
        enter = fadeIn() + slideInVertically { it },
        exit = fadeOut() + slideOutVertically { it},
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .padding(bottom = acutalPaddingBottom.value)
                .fillMaxWidth()
                .heightIn(min = 56.dp)
                .clip(RoundedCornerShape(8.dp))
                .background(LocalAppTheme.colors.run {
                    if (state.value?.isSuccess == true) checkGreen else error
                })
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = state.value?.text?.rememberAsAnnotatedString() ?: AnnotatedString(""),
                color = LocalAppTheme.colors.white,
                style = LocalAppTheme.typography.l14M,
            )
        }
    }
}