package kz.pyt.ui.widgets.checkables

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.compositeOver
import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider

data class CheckableState(
    val isEnabled: Boolean = true,
    val isChecked: Boolean = false,
)

internal class CheckablePreviewParameterProvider : CollectionPreviewParameterProvider<CheckableState>(
    collection = listOf(
        CheckableState(isChecked = false, isEnabled = true),
        CheckableState(isChecked = true, isEnabled = true),
        CheckableState(isChecked = false, isEnabled = false),
        CheckableState(isChecked = true, isEnabled = false),
    )
)

internal fun updateWithOverlayColor(
    enabledColor: Color,
    overlayColor: Color,
    isEnabled: Boolean
): Color {
    return if (isEnabled) {
        enabledColor
    } else {
        overlayColor.compositeOver(enabledColor)
    }
}
