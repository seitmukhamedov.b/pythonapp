package kz.pyt.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.mutableStateOf

class MainContentOverlayProvider {
    val composable = mutableStateOf<(@Composable () -> Unit)?>(null)
}

val LocalMainContentOverlayProvider = compositionLocalOf { MainContentOverlayProvider() }
