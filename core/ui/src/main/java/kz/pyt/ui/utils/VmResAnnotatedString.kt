package kz.pyt.ui.utils

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.buildAnnotatedString
import kz.pyt.ui.theme.Colors
import kz.pyt.ui.theme.LocalAppTheme

class VmResAnnotatedStringComposeWithScope(
    private val key: Any?,
    private val builder: AnnotatedString.Builder.(Context, Colors) -> Unit,
): VmRes.Compose<CharSequence> {

    @Composable
    override fun get(): CharSequence {
        val colors = LocalAppTheme.colors
        val context = LocalContext.current
        return remember(key, colors) {
            buildAnnotatedString {
                builder(context, colors)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is VmResAnnotatedStringComposeWithScope) return false

        if (key != other.key) return false
        if (builder != other.builder) return false

        return true
    }

    override fun hashCode(): Int {
        var result = key?.hashCode() ?: 0
        result = 31 * result + builder.hashCode()
        return result
    }
}
