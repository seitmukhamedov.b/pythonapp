package kz.pyt.ui.di

import kz.pyt.ui.widgets.dialog.DialogWithTextFieldViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val uiModule = module {
    viewModelOf(::DialogWithTextFieldViewModel)
}
