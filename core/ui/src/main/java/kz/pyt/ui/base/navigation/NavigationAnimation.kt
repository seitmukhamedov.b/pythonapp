package kz.pyt.ui.base.navigation

import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import dev.olshevski.navigation.reimagined.NavAction
import dev.olshevski.navigation.reimagined.NavTransitionScope
import dev.olshevski.navigation.reimagined.NavTransitionSpec
import kz.pyt.ui.base.Destination
import kz.pyt.ui.utils.motion.animation.materialFadeThrough
import kz.pyt.ui.utils.motion.animation.materialSharedAxisX
import kz.pyt.ui.utils.motion.animation.materialSharedAxisY
import kz.pyt.ui.utils.motion.animation.materialSharedAxisZ

object NavigationAnimation {
    fun slideVerticalFromTop() = materialSharedAxisY(
        forward = false,
        slideDistance = 300
    )
    fun slideVerticalFromBottom() = materialSharedAxisY(
        forward = true,
        slideDistance = 300
    )

    fun slideHorizontalFromEnd() = materialSharedAxisX(
        forward = true,
        slideDistance = 300
    )
    fun slideHorizontalFromStart() = materialSharedAxisX(
        forward = false,
        slideDistance = 300
    )

    fun scaleStart() = materialSharedAxisZ(
        forward = true,
    )

    fun scaleEnd() = materialSharedAxisZ(
        forward = false,
    )
    fun crossfade() = materialFadeThrough()
}

@ExperimentalAnimationApi
val NavigationTransitionSpec = object : NavTransitionSpec<Destination?> {

    override fun NavTransitionScope.getContentTransform(
        action: NavAction,
        from: Destination?,
        to: Destination?
    ): ContentTransform =
        (if (action is NavAction.Pop) from?.closeTransition(to) else to?.openTransition(
            action,
            from
        )) ?: NavigationAnimation.crossfade()

    override fun NavTransitionScope.toEmptyBackstack(
        action: NavAction,
        from: Destination?
    ): ContentTransform = NavigationAnimation.crossfade()

    override fun NavTransitionScope.fromEmptyBackstack(
        action: NavAction,
        to: Destination?
    ): ContentTransform = NavigationAnimation.crossfade()
}

