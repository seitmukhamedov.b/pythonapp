package kz.pyt.ui.widgets.checkables.switcher

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import kz.pyt.ui.ext.advanceShadow
import kz.pyt.ui.theme.LocalAppTheme
import kz.pyt.ui.widgets.checkables.CheckablePreviewParameterProvider
import kz.pyt.ui.widgets.checkables.CheckableState

private val switchHeight = 23.1.dp
private val switchWidth = 38.dp
private val gapBetweenThumbAndTrackEdge = 1.8.dp
private val thumbSize = 20.12.dp

@Composable
fun SwitchComponent(
    state: CheckableState,
    modifier: Modifier = Modifier,
    config: SwitchComponentConfig = SwitchComponentConfig.Default,
    onCheckedChange: ((Boolean) -> Unit),
) {
    val isChecked = state.isChecked
    // this is to disable the ripple effect
    val interactionSource = remember {
        MutableInteractionSource()
    }

    val disabledOverlayColor = LocalAppTheme.colors.white.copy(alpha = 0.55f)

    // for moving the thumb
    val alignment by animateAlignmentAsState(if (isChecked) 1f else -1f)

    val backgroundColor by animateColorAsState(
        if (isChecked) config.checkedTrackColor else config.uncheckedTrackColor,
        label = "",
    )
    val overlayColor = animateColorAsState(
        targetValue = if (state.isEnabled) {
            Color.Transparent
        } else {
            disabledOverlayColor
        },
        label = "SwitchComponent_overlayColor"
    )
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .clip(CircleShape)
            .size(
                width = switchWidth,
                height = switchHeight
            )
            .background(backgroundColor)
            .toggleable(
                value = isChecked,
                onValueChange = onCheckedChange,
                enabled = state.isEnabled,
                role = Role.Switch,
                interactionSource = interactionSource,
                indication = null
            )
    ) {
        // this is to add padding at the each horizontal side
        Box(
            modifier = Modifier
                .padding(
                    horizontal = gapBetweenThumbAndTrackEdge,
                )
                .fillMaxSize(),
            contentAlignment = alignment
        ) {
            Box(
                modifier = Modifier
                    .size(size = thumbSize)
                    .advanceShadow(
                        color = LocalAppTheme.colors.shadow,
                        borderRadius = 16.dp,
                        offsetY = 4.dp,
                        offsetX = 2.dp,
                    )
                    .background(
                        color = config.thumbColor,
                        shape = CircleShape
                    )
            )
        }
        Spacer(
            modifier = Modifier
                .fillMaxSize()
                .background(overlayColor.value)
        )
    }
}

@Composable
private fun animateAlignmentAsState(
    targetBiasValue: Float
): State<BiasAlignment> {
    val bias by animateFloatAsState(
        targetValue = targetBiasValue,
        animationSpec = spring(
            dampingRatio = 0.65f,
            stiffness = Spring.StiffnessMediumLow,
        ),
        label = ""
    )
    return remember {
        derivedStateOf { BiasAlignment(horizontalBias = bias, verticalBias = 0f) }
    }
}

@Composable
@Preview
private fun SwitchComponent_Preview(
    @PreviewParameter(CheckablePreviewParameterProvider::class) state: CheckableState
) {
    var currentState by remember {
        mutableStateOf(state)
    }
    SwitchComponent(
        state = currentState,
        onCheckedChange = {
            currentState = state.copy(
                isChecked = it
            )
        },
    )
}
