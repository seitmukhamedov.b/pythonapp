package kz.pyt.ui.widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kz.pyt.ui.theme.LocalAppTheme

@Composable
fun CommonTabIndicator(
    modifier: Modifier = Modifier,
    color: Color = LocalAppTheme.colors.primaryText
) {
    Spacer(
        modifier
            .padding(horizontal = 16.dp)
            .height(4.dp)
            .background(color, RoundedCornerShape(topStartPercent = 100, topEndPercent = 100))
    )
}