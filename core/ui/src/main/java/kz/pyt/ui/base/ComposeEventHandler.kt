package kz.pyt.ui.base

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import kotlinx.coroutines.flow.Flow
import kz.pyt.ui.DestinationController
import kz.pyt.ui.LocalDestinationController
import kz.pyt.ui.base.effect.baseEffectHandler
import kz.pyt.ui.base.navigation.NavigationEvent
import kz.pyt.ui.base.navigation.navigationHandler
import kz.pyt.ui.base.viewmodel.UiEffect
import kz.pyt.ui.base.viewmodel.UiEvent
import kz.pyt.ui.ext.collectInLaunchedEffectWithLifecycle
import kz.pyt.ui.widgets.snackbar.LocalSnackbarContentProvider

@Composable
fun ComposeEventHandler(
    event: Flow<UiEvent>,
    controller: DestinationController = LocalDestinationController,
    onNavigationEvent: (NavigationEvent.() -> Unit)? = null,
    handleEvent: (suspend UiEvent.() -> Unit)? = null,
) {
    val context = LocalContext.current
    event.collectInLaunchedEffectWithLifecycle { consumingEvent ->
        when (consumingEvent) {
            is NavigationEvent -> navigationHandler(
                event = consumingEvent,
                controller = controller,
                context = context,
                handler = onNavigationEvent
            )
            else -> handleEvent?.invoke(consumingEvent)
        }
    }
}

@Composable
fun ComposeEffectHandler(
    effect: Flow<UiEffect>,
    handleEffect: (UiEffect.() -> Unit)? = null,
) {
    val context = LocalContext.current
    val snackbarContentProvider = LocalSnackbarContentProvider.current
    effect.collectInLaunchedEffectWithLifecycle {
        baseEffectHandler(
            effect = it,
            handle = handleEffect,
            snackbarContentProvider = snackbarContentProvider,
            context = context,
        )
    }
}