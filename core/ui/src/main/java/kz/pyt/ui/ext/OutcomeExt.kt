package kz.pyt.ui.ext

import kz.pyt.ui.utils.VmRes
import kz.pyt.utils.outcome.Outcome

fun Outcome.Error.getError() = when (this) {
    Outcome.Error.ConnectionError -> VmRes.Str("")
    is Outcome.Error.ResponseError -> this.message?.let {
        VmRes.Str(it)
    } ?: VmRes.Str("")
    is Outcome.Error.UnknownError -> this.message?.let {
        VmRes.Str(it)
    } ?: VmRes.Str("")
}