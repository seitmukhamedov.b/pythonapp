package kz.pyt.ui.widgets

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kz.pyt.ui.R
import kz.pyt.ui.theme.LocalAppTheme

@Composable
fun CheckBox(
    isChecked: Boolean,
    modifier: Modifier = Modifier,
    shape: Shape = CircleShape,
    checkedColor: Color = LocalAppTheme.colors.checkGreen,
    uncheckedColor: Color = LocalAppTheme.colors.white,
    borderColor: Color = LocalAppTheme.colors.stroke,
    size: Dp = 24.dp,
) {
    val background by animateColorAsState(
        if (isChecked) checkedColor else uncheckedColor,
        label = "",
    )
    Box(
        modifier = modifier
            .size(size)
            .clip(shape)
            .border(
                if (isChecked) 0.dp else 1.dp,
                borderColor,
                shape
            )
            .background(background),
        contentAlignment = Alignment.Center,
    ) {
        CheckedCircleContent(
            isChecked = isChecked,
            parentSize = size,
        )
    }
}

@Composable
fun CheckBox(
    isChecked: Boolean,
    isCircle: Boolean = true,
    isCheckedLate: Boolean = false,
    background: Color = run {
        val color by animateColorAsState(
            LocalAppTheme.colors.run {
                if (isCheckedLate) bgLate else if(isChecked) checkGreen else white
            },
            label = "",
        )
        color
    },
) {
    val shape = if (isCircle) CircleShape else RoundedCornerShape(8.dp)
    CheckBox(
        isChecked = isChecked,
        shape = shape,
        checkedColor = LocalAppTheme.colors.run {
            if (isCheckedLate) bgLate else checkGreen
        },
    )
}

@Composable
private fun CheckedCircleContent(
    isChecked: Boolean,
    parentSize: Dp,
) {
    val iconSize = (parentSize * 2f) / 3f
    AnimatedVisibility(visible = isChecked) {
        Icon(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_check_16),
            contentDescription = null,
            tint = Color.Unspecified,
            modifier = Modifier.size(iconSize)
        )
    }
}
