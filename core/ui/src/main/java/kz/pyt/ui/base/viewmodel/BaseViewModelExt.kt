package kz.pyt.ui.base.viewmodel

import androidx.annotation.StringRes
import kz.pyt.ui.base.effect.ErrorEffect
import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.ext.getError
import kz.pyt.ui.utils.VmRes
import kz.pyt.utils.outcome.Outcome

fun BaseViewModel<*>.handleError(error: Outcome.Error) {
    setEffect {
        ErrorEffect(error.getError())
    }
}

fun BaseViewModel<*>.sendErrorMessage(error: String) {
    setEffect {
        ErrorEffect(VmRes.Str(error))
    }
}

fun BaseViewModel<*>.sendErrorMessage(@StringRes errorRes: Int) {
    setEffect {
        ErrorEffect(VmRes.StrRes(errorRes))
    }
}
