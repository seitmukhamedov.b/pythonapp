package kz.pyt.ui.base.effect

import android.content.Context
import kz.pyt.ui.base.effect.SnackbarEffect
import kz.pyt.ui.base.effect.ToastEffect
import kz.pyt.ui.base.viewmodel.UiEffect
import kz.pyt.ui.widgets.snackbar.SnackbarContentProvider

fun baseEffectHandler(
    effect: UiEffect?,
    context: Context,
    snackbarContentProvider: SnackbarContentProvider,
    handle: (UiEffect.() -> Unit)? = null
) {
    when(effect) {
        is ToastEffect -> effect.show(context)
        is SnackbarEffect -> effect.showSnackbar(snackbarContentProvider)
        else -> effect?.let { handle?.invoke(it) }
    }
}
