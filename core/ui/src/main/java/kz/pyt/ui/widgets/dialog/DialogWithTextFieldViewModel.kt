package kz.pyt.ui.widgets.dialog

import kz.pyt.ui.base.viewmodel.BaseViewModel
import kz.pyt.ui.base.viewmodel.UiState
import kz.pyt.ui.base.viewmodel.handleError
import kz.pyt.ui.screen.CommonTextFieldDialogDestination
import kz.pyt.utils.outcome.doOnError
import kz.pyt.utils.outcome.doOnSuccess

internal data class DialogTextFieldState(
    val loading: Boolean = false,
    val field: String = "",
    val isEnabled: Boolean,
) : UiState

internal class DialogWithTextFieldViewModel(
    private val behavior: CommonTextFieldDialogDestination.Behavior,
) : BaseViewModel<DialogTextFieldState>() {
    override fun createInitialState() = DialogTextFieldState(
        isEnabled = behavior.canBeEmpty
    )

    fun onClickButton() {
        launch {
            setState {
                copy(loading = true)
            }
            behavior.onClickButton(currentState.field)
                .doOnSuccess {
                    setState {
                        copy(loading = false)
                    }
                }
                .doOnError {
                    setState {
                        copy(loading = false)
                    }
                    handleError(it)
                }
        }
    }

    fun onUpdateTextField(value: String) {
        setState {
            copy(
                field = value,
                isEnabled = if (!behavior.canBeEmpty) {
                    value.isNotEmpty()
                } else {
                    true
                }
            )
        }
    }
}
