package kz.pyt.ui.widgets

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import kz.pyt.ui.theme.LocalAppTheme

@Composable
fun AnimatedCounter(
    text: String,
    modifier: Modifier = Modifier,
    style: TextStyle,
    color: Color = LocalAppTheme.colors.primaryText,
    transitionSpec: AnimatedContentTransitionScope<Char>.() -> ContentTransform = {
        slideInVertically {
            if (initialState > targetState) it else -it
        } togetherWith slideOutVertically {
            if (initialState > targetState) -it else it
        }
    },
) {
    var oldCount by remember {
        mutableStateOf(text)
    }
    SideEffect {
        oldCount = text
    }
    Row(modifier = modifier) {
        val oldCountString = oldCount
        for (i in text.indices) {
            val oldChar = oldCountString.getOrNull(i)
            val newChar = text[i]
            val char = if (oldChar == newChar) {
                oldCountString[i]
            } else {
                text[i]
            }
            AnimatedContent(
                targetState = char,
                transitionSpec = transitionSpec,
                label = ","
            ) { i ->
                Text(
                    text = i.toString(),
                    style = style,
                    softWrap = false,
                    color = color,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )
            }
        }
    }
}
