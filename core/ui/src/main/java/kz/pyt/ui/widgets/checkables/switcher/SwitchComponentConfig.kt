package kz.pyt.ui.widgets.checkables.switcher

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import kz.pyt.ui.theme.LocalAppTheme

interface SwitchComponentConfig {
    val checkedTrackColor: Color @Composable get
    val uncheckedTrackColor: Color @Composable get
    val thumbColor: Color @Composable get() = LocalAppTheme.colors.white

    data object Default : SwitchComponentConfig {
        override val checkedTrackColor: Color
            @Composable get() = LocalAppTheme.colors.main
        override val uncheckedTrackColor: Color
            @Composable get() = LocalAppTheme.colors.bg1
    }
}
