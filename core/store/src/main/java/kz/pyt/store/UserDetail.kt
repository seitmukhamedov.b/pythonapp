package kz.pyt.store

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.time.OffsetDateTime

@Parcelize
data class UserDetail(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val phoneNumberTeacher: String,
    val school: String,
    val classs: String,
    val address: String,
    val password: String,
) : Parcelable
