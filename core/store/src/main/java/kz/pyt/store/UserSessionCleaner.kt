package kz.pyt.store

interface UserSessionCleaner {
    suspend fun clean(authToken: String)

}
