package kz.pyt.store.di

import kz.pyt.store.UserSessionCleaner
import kz.pyt.store.UserStore
import kz.pyt.utils.getEncryptedSharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val storeModule = module {
    factory {
        androidContext().getEncryptedSharedPreferences("user_prefs")
    }
    singleOf(::UserStore) bind UserSessionCleaner::class
}
