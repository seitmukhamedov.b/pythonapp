package kz.pyt.store

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.MutableStateFlow
import kz.pyt.utils.GsonPreference
import kz.pyt.utils.Language
import kz.pyt.utils.StringPreference

class UserStore(
    sharedPrefs: SharedPreferences,
    gson: Gson,
) : UserSessionCleaner {

    var task1 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_1",
    )

    var task2 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_2",
    )

    var task3 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_3",
    )

    var task4 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_4",
    )

    var task5 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_5",
    )

    var task6 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_6",
    )

    var task7 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_7",
    )

    var task8 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_8",
    )

    var task9 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_9",
    )

    var task10 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_10",
    )

    var task11 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_11",
    )

    var task12 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_12",
    )

    var task13 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_13",
    )

    var task14 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_14",
    )

    var task15 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_15",
    )

    var task16 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_16",
    )

    var task17 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_17",
    )

    var task18 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_18",
    )

    var task19 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_19",
    )

    var task20 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_20",
    )

    var task21 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_21",
    )

    var task22 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_22",
    )

    var task23 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_23",
    )

    var task24 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_24",
    )

    var task25 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_25",
    )

    var task26 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_26",
    )

    var task27 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_27",
    )

    var task28 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_28",
    )

    var task29 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_29",
    )

    var task30 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_30",
    )

    var task31 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_31",
    )

    var task32 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_32",
    )

    var task33 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_33",
    )

    var task34 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_34",
    )

    var task35 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_35",
    )

    var task36 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_36",
    )

    var task37 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_37",
    )

    var task38 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_38",
    )

    var task39 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_39",
    )

    var task40 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_40",
    )

    var task41 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_41",
    )

    var task42 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_42",
    )

    var task43 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_43",
    )

    var task44 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_44",
    )

    var task45 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_45",
    )

    var task46 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_46",
    )

    var task47 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_47",
    )

    var task48 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_48",
    )

    var task49 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_49",
    )

    var task50 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_50",
    )

    var task51 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_51",
    )

    var task52 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_52",
    )

    var task53 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_53",
    )

    var task54 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_54",
    )

    var task55 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_55",
    )

    var task56 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_56",
    )

    var task57 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_57",
    )

    var task58 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_58",
    )

    var task59 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_59",
    )

    var task60 by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "task_60",
    )








    var accessToken by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "user_access_token",
    )

    var refreshToken by StringPreference(
        sharedPreferences = sharedPrefs,
        key = "user_refresh_token",
    )

    var userDetails: UserDetail? by GsonPreference(
        sharedPreferences = sharedPrefs,
        gson = gson,
        key = "user_details",
        type = object : TypeToken<UserDetail>() {}.type
    )


    private var _language: Language? by GsonPreference(
        sharedPrefs,
        gson,
        Language::class.java,
        defaultValue = Language.Kazakh
    )

    val languageFlow = MutableStateFlow(_language ?: Language.Kazakh)

    var language
        get() = languageFlow.value
        set(value) {
            languageFlow.value = value
            _language = value
        }

    fun clearUserSession() {
        accessToken = null
        refreshToken = null
        userDetails = null
        isLoggedFlow.value = false
    }

    val isLogged get() = !accessToken.isNullOrEmpty()


    val isLoggedFlow = MutableStateFlow(isLogged)
    override suspend fun clean(authToken: String) {
        clearUserSession()
    }

}
