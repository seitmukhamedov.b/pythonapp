package kz.pyt.network

import kz.pyt.store.UserStore
import okhttp3.Interceptor
import okhttp3.Response

class AppHeader {

    private companion object {
        private const val COOKIE = "cookie"
    }

    fun createAppHeaders(): Map<String, String> = mapOf(
        COOKIE to "_ga=GA1.1.1157572430.1718603655; _clck=q275gw%7C2%7Cfmp%7C0%7C1629; g_state={\"i_p\":1718610888884,\"i_l\":1}; marker_id_65379a42896b9b3785d33e92=f1c9551b-6423-479f-874a-7d7cf17d418f; SESSION=3c6096fe-cee6-4ff8-a334-4b58d5d2d305; ph_phc_LKNXMFCglPLUBhoCzNZ4r1crNe1Td2EGxzkgpxMmcVx_posthog=%7B%22distinct_id%22%3A%22warfacehefox125%40gmail.com%22%2C%22%24sesid%22%3A%5B1718608578776%2C%220190250c-5c45-7fab-93ed-a80d8f1aba9e%22%2C1718608485445%5D%2C%22%24epp%22%3Atrue%7D; _clsk=bvvmck%7C1718608696808%7C18%7C1%7Cn.clarity.ms%2Fcollect; _ga_Z3GW0401P2=GS1.1.1718608433.2.1.1718608704.0.0.0",
    )
}


class AppHeadersInterceptor(
    private val appHeader: AppHeader,
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        appHeader.createAppHeaders().forEach {
            requestBuilder.addHeader(it.key, it.value)
        }
        return chain.proceed(requestBuilder.build())
    }
}
