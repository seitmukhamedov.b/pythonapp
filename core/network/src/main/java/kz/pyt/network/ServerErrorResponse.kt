package kz.pyt.network

import com.google.gson.annotations.SerializedName

class ServerErrorResponse(
    @SerializedName("message", alternate = ["detail"]) val message: String?,
    @SerializedName("code") val code: String?,
    @SerializedName("status") val status: String?,
)
