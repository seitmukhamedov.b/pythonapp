package kz.pyt.network

import okhttp3.OkHttpClient
import org.koin.core.component.KoinComponent
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

class RetrofitProvider(
    okHttpClient: OkHttpClient,
    callAdapterFactory: CallAdapter.Factory,
    converterFactory: Converter.Factory,
) : KoinComponent {

    private val retrofitBuilder = Retrofit.Builder()
        .addCallAdapterFactory(callAdapterFactory)
        .addConverterFactory(converterFactory)
        .client(okHttpClient)

    val baseRetrofit: Retrofit by lazy {
        createRetrofit("https://www.jdoodle.com/")
    }

    fun createRetrofit(baseUrl: String, builder: Retrofit.Builder = retrofitBuilder): Retrofit {
        return builder.baseUrl(baseUrl).build()
    }
}