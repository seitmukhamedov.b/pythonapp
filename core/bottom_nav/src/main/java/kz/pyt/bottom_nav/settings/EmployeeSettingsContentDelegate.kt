package kz.pyt.employee.settings

import androidx.compose.runtime.Composable

interface EmployeeSettingsContentDelegate {

    @Composable
    fun Content()
}
