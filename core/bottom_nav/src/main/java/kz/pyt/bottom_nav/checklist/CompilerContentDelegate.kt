package kz.pyt.employee.checklist

import androidx.compose.runtime.Composable

interface CompilerContentDelegate {

    @Composable
    fun Content()
}