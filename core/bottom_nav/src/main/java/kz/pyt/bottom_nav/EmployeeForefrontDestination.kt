package kz.pyt.bottom_nav

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import kotlinx.parcelize.Parcelize
import kz.pyt.core.forefront_nav.R
import kz.pyt.employee.checklist.CompilerContentDelegate
import kz.pyt.employee.home.HomeContentDelegate
import kz.pyt.employee.settings.EmployeeSettingsContentDelegate
import kz.pyt.forefront_nav.ForefrontDestination
import kz.pyt.ui.DestinationController
import org.koin.compose.koinInject

@Stable
sealed interface EmployeeForefrontDestination : ForefrontDestination {

    @Parcelize
    object Home : EmployeeForefrontDestination {

        override val icon: Int get() = R.drawable.ic_home_vector_28
        override val isEntryPoint: Boolean get() = true

        @Composable
        override fun Content(controller: DestinationController) {
            val delegate = koinInject<HomeContentDelegate>()
            delegate.Content()
        }
    }
    @Parcelize
    object Compiler : EmployeeForefrontDestination {

        override val icon: Int get() = R.drawable.ic_vector_27
        override val isEntryPoint: Boolean get() = false

        @Composable
        override fun Content(controller: DestinationController) {
            val delegate = koinInject<CompilerContentDelegate>()
            delegate.Content()
        }
    }

    @Parcelize
    object Settings : EmployeeForefrontDestination {

        override val icon: Int get() = R.drawable.ic_profile_29
        override val isEntryPoint: Boolean get() = false

        @Composable
        override fun Content(controller: DestinationController) {
            val settingsDelegate = koinInject<EmployeeSettingsContentDelegate>()
            settingsDelegate.Content()
        }
    }

}
