package kz.pyt.employee.home

import androidx.compose.runtime.Composable

interface HomeContentDelegate {

    @Composable
    fun Content()
}
