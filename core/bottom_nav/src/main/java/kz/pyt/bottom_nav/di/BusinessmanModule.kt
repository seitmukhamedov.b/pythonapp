package kz.pyt.employee.di

import kz.pyt.employee.ForefrontDestinationsDelegateImpl
import kz.pyt.forefront_nav.ForefrontDestinationsDelegate
import org.koin.dsl.module

val bottomNavModule = module {
    factory<ForefrontDestinationsDelegate> {
        ForefrontDestinationsDelegateImpl()
    }
}
