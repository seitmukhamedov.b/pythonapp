package kz.pyt.employee

import kz.pyt.bottom_nav.EmployeeForefrontDestination
import kz.pyt.forefront_nav.ForefrontDestination
import kz.pyt.forefront_nav.ForefrontDestinationsDelegate

internal class ForefrontDestinationsDelegateImpl : ForefrontDestinationsDelegate {
    override fun getDestinations(): List<ForefrontDestination> = listOf(
        EmployeeForefrontDestination.Home,
        EmployeeForefrontDestination.Compiler,
        EmployeeForefrontDestination.Settings,
    )
}