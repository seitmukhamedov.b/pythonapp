package kz.pyt.forefront_nav

import android.content.Intent

interface MainActivityIntentReceiver {
    fun getIntent(): Intent
}
