package kz.pyt.utils.ext

import android.text.TextUtils
import android.webkit.MimeTypeMap
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.withStyle
import java.util.Locale

val imageExt = setOf(
    "jpeg",
    "png",
    "jpg",
    "webp",
)

val videoExt = setOf(
    "mp4",
    "mpeg",
    "heic",
    "heiv",
    "webm",
    "avi",
    "gif",
    "wmv",
    "mov",
    "MP4",
)

val audioExt = setOf(
    "m4a",
)

fun String.capitalizeWithLocale() = replaceFirstChar {
    if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
}

fun String.isEmailValid(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.withAtSign() = if (startsWith('@')) this else "@$this"

fun String.onlyDigits() = filter {
    it.isDigit()
}

fun String.getMimeType(): String? {
    var type: String? = null
    val extension = MimeTypeMap.getFileExtensionFromUrl(this)
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    return type
}

fun String.isImagePath(): Boolean {
    val extension = split(".").lastOrNull()
    return extension?.lowercase() in imageExt
}

fun String.isVideoPath(): Boolean {
    val extension = split(".").lastOrNull()
    return extension?.lowercase() in videoExt
}

fun String.isPdf(): Boolean {
    val extension = split(".").lastOrNull()
    return extension?.lowercase() == "pdf"
}

fun String.isGif(): Boolean {
    val extension = split(".").lastOrNull()
    return extension?.lowercase() == "gif"
}

fun String.isAudioPath(): Boolean {
    val extension = split(".").lastOrNull()
    return extension?.lowercase() in audioExt
}

fun String.getColor(): Color {
    return Color(android.graphics.Color.parseColor("#FF$this"))
}

fun AnnotatedString.Builder.withColor(
    color: Color,
    block: AnnotatedString.Builder.() -> Unit,
) {
    withStyle(
        SpanStyle(
            color = color,
        )
    ) {
        block()
    }
}

val String.withBearer get() = "Bearer $this"
