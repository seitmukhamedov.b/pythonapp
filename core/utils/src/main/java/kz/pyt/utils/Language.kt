package kz.pyt.utils

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.Locale

@Parcelize
enum class Language(
    val code: String,
) : Parcelable {
    @SerializedName("Kazakh")
    Kazakh(code = "kk");

    companion object
}

fun Language.Companion.fromLocale(locale: Locale) = when (locale.toLanguageTag().lowercase()) {
    else -> Language.Kazakh
}
