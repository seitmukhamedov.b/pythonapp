package kz.pyt.local.domain

fun interface DownloadFileProgressListener {
    fun onProgress(progress: Float)
}
