package kz.pyt.local.di

import kz.pyt.local.data.LocalStorageImpl
import kz.pyt.local.domain.LocalStorage
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val localStorageModule = module {
    single<LocalStorage> { LocalStorageImpl(androidContext()) }
}
